Map mapModele = {
  "Titre": ["titre", "note"],
  "def": ["def", "contenu de la def"],
};

//askip le plus léger

testTitre(Map map) {
  int nbTitre = 0;
  List<String> erreurs = [];
  map.forEach((key, value) {
    if (key.toString().contains("Titre")) {
      nbTitre += 1;
      if (value.runtimeType.toString() != "List<String>") {
        erreurs.add("\x1B[31merreur : $key doit être une liste de string\x1B[0m");
      }
      if (value.length != 2) {
        erreurs.add("\x1B[31merreur : $key doit avoir une length égal à 2\x1B[0m");
      }
    }
  });
  if (erreurs.length == 0) {
    return "\x1B[32m Titre ($nbTitre) : ok\x1B[0m";
  } else {
    return erreurs.join("\n");
  }
}

void main() {
  Map map = mapModele;
  print(testTitre(map));
}
