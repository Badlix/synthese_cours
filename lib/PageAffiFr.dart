import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PageAffiFr extends StatefulWidget {
  PageAffiFr(this.changeTheme, this.matiereSelect, this.nomCategorie, this.page);
  final Function changeTheme;
  final String matiereSelect;
  final String nomCategorie;
  final String page;
  final Map p1 = {
    //elle était déchaussé... FINI
    "txt": "un texte",
    "Analyse": {
      "INTRO":
          "romantisme 19ème siècle = appel aux sentiments / poèsie crée par les grec avec mythe d'Orphé, genre qui évolu au fil des siècles, peut etre lyricque ou polémique\nVH homme politique + homme de lutte connu pour avoir exploiter tt les genre littéraire -> chef de file du romantisme en France\nles comtemplations = aspect autobiographique versifié -> en parti rédiger pdt l'exil de VH\nDans cet extrait VH rencontre une femme",
      "LECTURE": "",
      "PROB": "En quoi ce poème est la représentation d'un coup de foudre",
      "PLAN": "v1à3/découverte\nv4à13/avance de VH\n14à16/aboutissement de la rencontre",
      "1 Partie":
          "\nle poème évoque un souvenir -> imparfait descriptif, mise en évidence de la jeune femme dont VH se souvient",
      "v1":
          "'elle' sera repris anaphoriquement sut tt le poème -> identité inconnu de la femme\nla descri porte sur le physique avec 2 adj accentué, 1 à la césure 'déchaussée', et 1 à la rime 'décoiffé' -> assonance en 'é' + time interne -> la femme est naturel et en dehors des conventions sociale du 19ème siècle -> confirmer avec le v2 intégré dans le cadre naturel 'parmi'",
      "v2": "déconstruction de l'alexandrin (3:3:6) -> il se ramemore avec une exactitude tous les détails",
      "v3":
          "mise en scène du poète lui-mm avec 'moi', 'je'\n2eme émistiche = effet de la femme produit sur VJ -> 'fée' = caractère hyperbolique et métaphorique -> semble irréelle, rime suffisante entre 'décoiffé' et 'fée' -> aspect sur-naturel; représentation du coup de foudre; pouvoir de séduction",
      "Transition 1-2": "Suite à ce coup de foudre, VH fais des avances à cette jeune fille",
      "2 Partie":
          "interrogation direct (v4,7,8) -> tutoiement -> but de creer une intimité -> anaphore de 'veux-tu' -> pour impliquer la jeune femme avec vb de volonté / -> il lui propose de s'isoler et se justifie v7; sers de prétexte pour introduire la verbe 'aimer' = état d'esprit de VH\nla fille ne rep jms avec des paroles -> rép avec le regard -> jeux de séduction 'regarder' (v5,10), 'regard', (v5)",
      "v5":
          " grand pouvoir que la femme exerce sur VH avec l'hyperbole 'regard supreme' qui créer un lien de cause-conséquence avec le vb 'aimer'\n-> la jeune femme ne s'offusque pas de la proposition de VH -> accentu son hors-convention -> périphrase 'la belle folatre' qui semble être en antithèse avec 'pensive' -> elle réfléchit à la proposition",
      "v6":
          "on comprend que l'attirance est réciproque tous les sens vont ensuite être sollicité, vu avec 'regard' et 'beauté'; ouie avec l'exclamation v12; le toucher avec métaphore v13 -> met en évidence le désir entre les perso et d'hérotiser le paysage\nphrases exclamatives servent à amplifier lyrisme du poème la nature semble se rendre complice des 2 perso avec le crescendo dans la prop de VH: 'dans les champs','sous les arbres profonds','au fond des bois' -> lieux de + en + intime pour favoriser le rapprochement entre les 2 persos",
      "v13": "métaphore est annonciatrice de l'acte entre les 2 perso",
      "Transition 2-3": "Les avances faites par VH vont finalement aboutir",
      "3 Partie":
          "'je vis venir à moi' -> consentement de la jeune fille; présence de l'enjambement pour montrer que la fille est totalement intégré au décor/nature",
      "v15":
          "rythme thernaire -> insiste sur la beauté et sur son état d'esprit -> confirme consentement car termes mélioratif",
      "v16":
          "l'effet va crescendo, insouciance de la jeune femme, 'cheveux dans ses yeux' -> rapelle 'décoiffée' et 'riant au travers' -> montre plaisir\ntt le poème était rempli d'allitération fricatives pour évoquer le contraste de légéreté qui se dégage de cette rencontre",
      "CONCLUSION":
          "VH se rappelle d'un souvenir heureux -> rencontre de jeunesse marquée par la réciprocité des sentiments. Dans ce poème le lyrisme fait référence à des sentiments heureux mais ce n'est pas toujours le cas avec VH"
    }
  };
  final Map p2 = {
    //melancholia FINI
    "txt": "texte",
    "Analyse": {
      "INTRO":
          "romantisme 19ème siècle = appel aux sentiments / poèsie crée par les grec avec mythe d'Orphé, genre qui évolu au fil des siècles, peut etre lyricque ou polémique\nVH homme politique + homme de lutte connu pour avoir exploiter tt les genre littéraire -> chef de file du romantisme en France\nles comtemplations = aspect autobiographique versifié -> en parti rédiger pdt l'exil de VH\nlivre III = les luttes et les rêves -> évoque un engagement explicitedu poète, les cause qu'il défend\ncet extrait est un pamphlet (écrit satirique aggresif dirigé contre qqn ou une institution)",
      "LECTURE": "",
      "PROB": "En quoi le registre pathétique permet-il la d'énonciation du travail des enfants",
      "PLAN": "1à3/descri des enfants\n4à16/descri travail\n17à20/prise de position explicite de VH",
      "1 Partie":
          "\n3 questions rhétorique -> insiste sur leur jeune âge avec périphrase\nenfants sensé être loin des préoccupations de la société",
      "1e émistiche":
          "évoque caractéristique lié aux enfants (incconcence, pureté)\n'Les enfants' -> utilisation du pluriel pour montrer qu'ils sont nombreux",
      "2e émistiche":
          "introduit aspect paradoxale à l'enfance -> 'pas un seul en rit' = souffrance psychologique / 'la fièvre maigrit' = souffrance physique / 'seul' = absence de protection\nassonance de voyalle nasale + consonne fricative montre la souffrance et leur état de vulnérabiblité",
      "Transition 1-2": "pas noté",
      "2 Partie :\nv4": "rep aux questions de la partie 1 + marqueur temporel de l'aube au soir -> action répétitive",
      "v5-6":
          "parrallélisme 'mm prison/mm mouvement' -> enfant agissent comme des automate/aucune possibilité d'échapper à leur condition\naspect renforcé avec métaphore de prison -> semblent être incarcéré (alors qu'ils n'ont commis aucune faute -> injuste)",
      "v4.":
          "marqueur de lieux 'sous les moules' pour donner l'impression que les enfants sont écrasé / amplifié par enjambement 7-8 avec métaphore filé 'monstres hideux', 'mâche' -> vision des enfants (ils percoivent cette machines comme une menace)",
      "v9":
          "parrallélisme avec antithèse ('innocent-bagne')/('ange-enfer'); bagne et enfer = renforce effet péjoratif du travail; innocent et ange = description des enfants (associé à la pureté) susciter pitié du lecteur, cela va être renforcé à partir du v12 et v14 -> le poème prend une tonalité élégiaque",
      "v10":
          "alexandrins coupé en 3 -> reproduire rythme de travail des enfants; parrallélisme insiste sur métaux (fer..etc) -> ils sont enfermés avec cette machine et ne voit jms la lumière du jour",
      "v11": "parrallélisme 'jms on ne s'arrete jms on ne joue -> leur journée est rythmé que par le travail",
      "v13": "paradoxe (début du jour - déjà bien las)",
      "v15-16":
          "apostrophe 'notre père'; travail contre nature; contre volonté divine\nantithèse 'petit' et 'homme', enfant victime des décision des adultes",
      "Transition 2-3":
          "VH est témoins de l'exploitation des enfants ce qui va donner lieu à sa prise de position explicite",
      "v17": "exclamation -> traduit pensée de VH -> 'infâme' = péjoratif",
      "v18": "exclamation; VH veut aller à l'essentiel, ce travail empêche les enfants de grandir normalement",
      "v19-20":
          "rythme saccadé lien avec les mouvements saccadésdu travail des enfants, insité sur l'état morbide de la situation oxymore (souffle étouffant) + fricative + verbe 'tuer' mis au centre -> l'issu qui est reservé à ces enfants est la mort -> insiste que l'effet de contre-nature de ce travail avec chiasme v20 -> travail détruit aspect physique et moral",
      "v21":
          "VH veut rendre son propos irréfutable; avec présent 'c'est' et superlatif 'le plus certain'\nparrallélisme finale introduit paradoxe pour montrer que nos systèmes de valeurs sont bouleversé par ce travails des enfants",
      "CONCLUSION": "Hugo fait un phamphlet pour dénoncer le travail des enfants",
      "OUVERTURE":
          "On sait aussi que VH a bcp de manière différente de s'engager comme avec des romans avec 'le dernier jour d'un condamné'"
    }
  };
  final Map p3 = {
    //livre 4, poème 4, FINI
    "txt":
        "Oh ! je fus comme fou dans le premier moment\nHélas ! et je pleurai trois jours amèrement.\nVous tous à qui Dieu prit votre chère espérance,\nPères, mères dont l’âme a souffert ma souffrance\nTout ce que j’éprouvais, l’avez-vous éprouvé ?\nJe voulais me briser le front sur un pavé ;\nPuis je me révoltais, et par moments, terrible,\nJe fixais mes regards sur cette chose horrible,\nEt je n’y croyais pas, et je m’écriais : Non !\n-Est-ce que Dieu permet de ces malheurs sans nom ?\nQui font que dans le cœur le désespoir se lève ?-\nIl me semblait que tout n’était qu’un affreux rêve,\nQu’elle ne pouvait pas m’avoir ainsi quitté,\nQue je l’entendais rire en la chambre à côté,\nQue c’était impossible enfin qu’elle fût morte,\nEt que j’allais la voir entrer par cette porte !\n\nOh ! que de fois j’ai dit : Silence ! elle a parlé !\nTenez ! voici le bruit de sa main sur la clé !\nAttendez ! elle vient ! laissez-moi, que j’écoute !\nCar elle est quelque part dans la maison sans doute !\n\nJersey, Marine-Terrace, 4 septembre 1852",
    "Analyse": {
      "INTRO":
          "romantisme 19ème siècle = appel aux sentiments / poèsie crée par les grec avec mythe d'Orphé, genre qui évolu au fil des siècles, peut etre lyricque ou polémique\nVH homme politique + homme de lutte connu pour avoir exploiter tt les genre littéraire -> chef de file du romantisme en France\nDans ce poème VH évoque la mort de sa fille Léopoldine qui l'a bcp marqué\nle recueil des Comtemplations a une fonction auto-biographique et rend hommage à sa fille surtout dans le livre 4\nCe poème est un poème tombeau et il appartient au registre élégiaque car il exprime la mélancolie et la réaction de Vh face à la mort de sa fille",
      "LECTURE": "",
      "PRIB": "En quoi le registre élégiaque permet-il a Hugo d'exprimer ses différents sentiments ?",
      "PLAN":
          "1à5/apostrophe de VH à ses lecteurs \n6à16/ état d'esprit de Vh après la mort de Léopoldine\n17à20/discour direct",
      "1 Partie :\nv1-v5": "Vh s'adresse à ses lecteurs en les apostrophants pour leurs partager sa souffrance",
      "v1-v2": "'Oh' et 'Hélas' -> registre élégiaque + souffrance renforcer avec consonne nasale",
      "?":
          "'1er moment', '3j' -> marqueur temporel montre que le premier moment à durer 3j + complémentarité avec 'amerement' et 'moment'",
      "v1":
          "Vh rapporte son chagrin à la folie 'oh je fus comme fou' -> veut se representer comme qqn plus capable de raisonner",
      "v3-v5":
          "interpelle les lecteurs 'vous', 'pères, mères', pour généraliser sa souffrance + deux dérivation v4-5 termer utilisait pour parler de Vh et des lecteurs -> pas de dif entre VH et les autres",
      "v3": "euphenisme 'chère espèrance' -> métonymie -> Dieu devient responsabe d'un acte illégitime",
      "Transition 1-2":
          "Après avoir apostrophé ses lecteurs VH va ns décrire son état d'esprit après la mort de Léopoldine",
      "2 Partie :\nv6":
          "récurrence de la 1ère personne 'je', 'me', 'mes' / occlusive 'b' et 'p' -> envie suicidaire du poète",
      "v7": "rythme fortement saccadé; 'terrible', 'horrible' -> rime riche",
      "v8": "'chose' -> indéfini -> VH n'arrive pas à qualifier ce dont il vaut parler",
      "v9": "déni avec les 2 négations / m'écriais = violence verbale",
      "v10-11": "déni va mm jusqu'à remettre en question l'action divine avec l'enjambement",
      "v11": "personnalisation désespoir; souffrance prend le dessus sur tt le reste",
      "v12-16":
          "sucession de subordonné avec un parrallélisme qui aborde les phrases sucessive / 'affreux rêve' -> oxymore, il n'est pas certains de la réalité de l'événement",
      "v13-v15":
          "évoque la mort de L avec crescendo 'quitter' -> 'mort' / Vh n'admet pas la mort de L (car phrase négative) en antithèse avec phrase affirmative -> vb action qui évoque la vie",
      "v14-16": "antithèse phrase affirmative -> verbe",
      "Transition 2-3": "Après avoir exposé ses différents états d'esprit VH confirma Son déni grâce au discour direct",
      "3 Partie\n":
          "chacun des vers est un indicateur pour justifier l'état proche de la folie de H / evenement récurrent + deuil long 'que de fois' / isotopie de l'ouie 'parler', 'bruit','j'écoute'",
      "v20":
          "chercher a trouver une preuve de la présence de L -> recherche physique de L + vb sans doute -> tellement convaincu qu'il donne des ordres à son entourage 'silence'",
      "v18":
          "impératif; le ton est injonctif; tellement convaincu de la présence de L qui arriverai à renier ceux qui sont vivant autour de lui pour se concentrer sur sa fille décédé et se bouleversement mental  est aussi caractérisé par l'écriture des vers -> tt les alexandrins sont destructuré -> marques différentes phase par lesquels il est passé => tte ces phases permette d'associer cette souffrance avec une forme de folie",
      "CONCLUSION":
          "Grâce au registre élégiaque, VH réussi à appostropher ses lecteurs à leur décrire son état d'esprit et à associer sa souffrance à une certaine forme de folie",
      "OUVERTURE":
          "Le poème 'demain dès l'aude' montre que VH va accepter la mort de L et il fera une introspection en se rendant sur la tombe de sa fille"
    }
  };
  final Map r1 = {
    //portrait de Julien
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    "txt":
        "« Eh bien, paresseux ! tu liras donc toujours tes maudits livres, pendant que tu es de garde à la scie ? Lis-les le soir, quand tu vas perdre ton temps chez le curé, à la bonne heure. »Julien, quoique étourdi par la force du coup, et tout sanglant, se rapprocha de son poste officiel, à côté de la scie. Il avait les larmes aux yeux, moins à cause de la douleur physique, que pour la perte de son livre qu'il adorait.« Descends, animal, que je te parle. » Le bruit de la machine empêcha encore Julien d'entendre cet ordre. Son père qui était descendu, ne voulant pas se donner la peine de remonter sur le mécanisme, alla chercher une longue perche pour abattre des noix, et l'en frappa sur l'épaule. A peine Julien fut-il à terre, que le vieux Sorel, le chassant rudement devant lui, le poussa vers la maison. Dieu sait ce qu'il va me faire ! se disait le jeune homme. En passant, il regarda tristement le ruisseau où était tombé son livre ; c'était celui de tous qu'il affectionnait le plus, le Mémorial de Sainte-Hélène.Il avait les joues pourpres et les yeux baissés. C'était un petit jeune homme de dix-huit à dix-neuf ans, faible en apparence, avec des traits irréguliers, mais délicats,et un nez aquilin. De grands yeux noirs, qui, dans les moments tranquilles, annonçaient de la réflexion et du feu, étaient animés en cet instant de l'expression de la haine la plus féroce. Des cheveux châtain foncé, plantés fort bas, lui donnaient un petit front, et dans les moments de colère, un air méchant. Parmi les innombrables variétés de la physionomie humaine, il n'en est peut-être point qui se soit distinguée par une spécialité plus saisissante. Une taille svelte et bien prise annonçait plus de légèreté que de vigueur. Dès sa première jeunesse, son air extrêmement pensif et sa grande pâleur avaient donné l'idée à son père qu'il ne vivrait pas, ou qu'il vivrait pour être une charge à sa famille. Objet des mépris de tous à la maison, il haïssait ses frères et son père ; dans les jeux du dimanche, sur la place publique, il était toujours battu.",
    "Analyse": {
      "INTRO":
          "Définition du roman qui devient un genre populaire au XIX° et qui depuis est le genre le plus lu.  Stendhal est un écrivain français du 19e, son vrai nom est Henri Beyle, Présentation rapide de l'oeuvre:Stendhal s'est inspiré de faits divers et de la situation sociale et politique de son temps pour écrire Le Rouge et le Noir; ce roman s'inscrit dans la lignée des romans d'apprentissage puisque le jeune Julien Sorel va s'émanciper grâce à ses différentes rencontres.Présentation de l'extrait: le chapitre 4 du livre 1 est la présentation du personnage principal du roman. Julien Sorel est le fils d'un paysan en tous points opposés avec lui et ses frères, alors qu'il était de garde à l'usine, il se fait surprendre par son père en train de lire, ce qui donne lieu à une confrontation entre les deux personnages et à une description du protagoniste.",
      "LECTURE": "",
      "PROB": "? bruh",
      "PLAN": "'Eh bien...Saint Héléne'/La confrontation avec le père\n'Il avait...tjrs battu'/portrait de J",
      "Analyse":
          "La confrontation entre les deux personnages débute par un discour direct qui met en place immédiatement un rapport de force dans lequel le père se révèle dominant  , d'une part car il estle seul à parler, Julien ne doit pas oser lui répondre et d'autre part par la violence des propos tenus. En effet, les paroles du père sont marquées par des impératifs 'Lis-les' ou 'Descends' afin de rendre le propos autoritaire , à cela s'ajoutent les périphrases 'paresseux', 'animal' visant à dénigrer Julien, tous les propos tenus par le père mettent en valeur le fait qu'il ne partage pas les intérêts de son fils et ne les comprend pas d'où la dévalorisation permanente présente dans le lexique péjoratif: ?, Dans les deux cas, il évident que le père Sorel reproche à son fils de ne pas travailler BRUH"
    }
  };
  final Map r2 = {
    //livre 1, chap 9
    "txt": "texte",
    "Analyse": {
      "INTRO":
          "Définition du roman qui devient un genre populaire au XIX° et qui depuis est le genre le plus lu.  Stendhal est un écrivain français du 19e, son vrai nom est Henri Beyle, Présentation rapide de l'oeuvre:Stendhal s'est inspiré de faits divers et de la situation sociale et politique de son temps pour écrire Le Rouge et le Noir; ce roman s'inscrit dans la lignée des romans d'apprentissage puisque le jeune Julien Sorel va s'émanciper grâce à ses différentes rencontres. Dans cet extrait Julien s'est fixé comme mission de prendre la main de Madame de Rénal",
      "LECTURE": "",
      "PROB": "Comment Standhal met-il en avant le caractère ambitieux et les valeurs des personnages ?",
      "PLAN":
          "1à9/l'hésitation de Julien\n10à17/actions de chacun des personnages\n18à23/la réussite de l'bjectif que Julien s'était fixé",
      "l-1/9":
          "première partie de l'extrait montre le personnages dans un état d'angoisse face à ce qu'il a prévu -> isotopie avec termes péjoratif et hyperbolique 'mortelle angoisse', 'voilence', 'altéré'.. -> montre que pour lui l'enjeu est important",
      "l-1/3":
          "la phrase hyperbolique en association avec le discourt indirect libre traduisent les pensées de Julien 'Que de fois ne désira-t-il pas voir survenir à Mme de Rénal suggérer la manque de rationalité du personnages -> il attend un secour extérieur pour éviter de devoir accomplir son objectif\nantithèse entre 'jardin' et 'maison' -> jardin = lieu d'accomplissement de la mission et 'maison' = endroit dans lequel il n'aura pas l'opportunité de céder à la tentation",
      "l-3/6":
          "son anxiété se manifeste également par le ton de sa voix 'pour que sa voix ne fût pas profondement altérer' -> lien de cause/conséquence -> met en évidence une manifestation physique du trouble\nil est trop centré sur lui pour penser aux sentiments de madame de Rénal comme le prouve la négation totale 'ne s'en aperçut point' ou encore dans la focalisation omnisciente faisant état du dilemme 'l'affreux combat que le devoir livrait à la timidité était trop pénible pour qu'il fut en état de rien observer hors de lui-même' -> la terme de 'devoir' traduit d'une ironie du narrateur qui se moque du personnage principal",
      "l-4":
          "une seule alusion est faite à madame de Rénal 'la voix de Mme de Rénal devint tremblante aussi' -> témoigne que Mme de Rénal n'est pas insensible aux avances de J",
      "l-7/9":
          "dernière étape de monologue = ultimatum qu'il se lance à lui même 'au moment où dix heures sonneront' -> marqueur temporel dominent et soulignent les étapes de calcul de J, de plus les futurs de certitude en lien avec l'alternative mise en place avec la conjonction de coordination 'ou', la métaphore hyperbolique finale 'me brûler la cervelle' insistent sur la détermination et les choix radicaux des personnages",
      "?":
          "dans cette première partie , J cumule les caractéristiques du héros et l'anti-héro, preuve du mal être du personnage et de sa difficulté à trouver sa place dans la société",
      "Partie 2": "",
      "l-10":
          "ryhtme binaire et marqueur de temps 'après un dernier moment d'attente et d'anxiété' qui résume à eux seul tte les émotions du personnages",
      "l-12/17":
          "marqueurs de temps 'dix heures sonnèrent', 'chaque coup de cloche', 'enfin comme le dernier coup retentissait' qui détermine les actions du personnage",
      "l-10/13":
          "sentiments hyperbolique 'l'excès de l'émotions', la comparaison 'comme hors de lui' -> suggère perso incapable de se controler -> diriger par force supérieur -> sous entendu par 'fatale'",
      "l-15/17":
          "actions s'enchainent avec rapidité 'il étendit la main...prit...retira', les passé simple accentu cette rapidité\nLa résistance de Mme de Rénal se montre avec 'la retira aussitot' -> R affolé qu'on découvre cette action car fidélité primordial pour elle\nla dernière proposition marqué par un connecteur d'opposition 'mais enfin cette main lui resta' prouve que J est victorieux et que Mme de Rénal à capitulé",
      "l-16":
          "métaphore + pléonasme 'de froideur glaciale' connote la froideur du cadavre -> ref devient symbolique -> une partie de Mme Rénal vient de mourir dans ce processus. Elle a perdu ses repère et ses valeurs.",
      "Partie 3": "",
      "l-18/19":
          "la dernière étape de l'extrait insiste sur les conséquences sur l'âme de J, on commence par évoquer les sentiments -> hyperbole 'son âme fut inondé de bonheur' souligne qu'il ne controle plus ses emotions\non insiste sur les raisons de ces sentiment avec une opposition 'non qu'il aimat...mais son supplice' -> il est évidenet que J n'agit que par intérêt personnel, il se sent fier d'avoir accompli l'objectif qu'il s'était donné",
      "l-19/20":
          "il reste dans le calcul avec la subordonné conjonctive 'pour que Mme Derville ne s'aperçut de rien'\nrythme binaire quelifiant la voix d'éclatant et forte et en antithèse avec le début de l'extrait dans lequel elle est 'profondemment altéré' -> J est donc en confiance contrairement à Mme de Rénal 'celle de Mme de Rénal au contraire trahissait tant d'émotions' -> l'hyperbole souligne la multitude de sentiments par lesquels la jeune femme passe et la difficulté à les maitriser",
      "l-21/23":
          "le discour indirect libre qui termine l'extrait 'si Mme de Rénal...acquis' -> met en évidence l'ambition du personnages. il ne se concentre que sur ses sentiments passé 'je vais retomber dans cette position affreuse où j'ai passé la journée', J est en quête réussite"
    }
  };
  final Map r3 = {
    //livre 2, chap 44 FINI
    "txt": "texte",
    "Analyse": {
      "INTRO":
          "Définition du roman qui devient un genre populaire au XIX° et qui depuis est le genre le plus lu.  Stendhal est un écrivain français du 19e, son vrai nom est Henri Beyle, Présentation rapide de l'oeuvre:Stendhal s'est inspiré de faits divers et de la situation sociale et politique de son temps pour écrire Le Rouge et le Noir; ce roman s'inscrit dans la lignée des romans d'apprentissage puisque le jeune Julien Sorel va s'émanciper grâce à ses différentes rencontres. Dans cet extrait Julien se remet en question peut avant sa peine de mort",
      "LECTURE": "",
      "PROB": "Quelles sont les différentes étapes de son monologue intérieur ?",
      "PLAN":
          "1à6/Le sentiment de solitude\n7à16/l'expression du mal du siècle\n17à23/Les regrets de Julien vis à vis de Madame Rénal",
      "l-1":
          "2 phrases exclamatives non-verbales suivies d'oposiopèse -> montre l'expression des sentiments associé à la souffrance -> présence du registre élégiaque",
      "l-2/6":
          "reflexion sur la solitude (dérivation d'isolé + substantif 'isolement' -> incarcération représente isolement physique et moral qui favorise l'introspection + le monologue intérieur",
      "l-2/5":
          "variation des temps verbaux + précisement le passage au présent 'deviens/suis isolé' -> représente la situation actuelle dans laquelle J se trouve tandis que l'imparfait et le plus que parfait renvoient à une antériorité -> reflexion sur l'existence telle que J l'a mené",
      "l-3":
          " réf au passé avec hyperbole 'j'avais la puissante idée du devoir' -> montre une valer=ur passé + marque ce qui dirigeait autrefois son existence",
      "l-4/5":
          "métaphore filée de l'orage avec 'vacillait', 'agité' -> montre l'instabilité en lien avec les sentiments + fais rèf au coup de foudre avec Madame de Rénal",
      "l-5":
          "déterminant indéfini 'un homme' -> introduit par la restrictive 'qu'un' est un opposition avec 'mais je n'étais pas emporté' -> montre qu'il était ordinaire mais + un manque d'objectivité de sa part puisqu'il a tiré sur Madame de Rénal et donc se laisser dirifer par ses émotions.",
      "Transition 1-2":
          "après avoir vu l'expression des sentiments de solitude de J, nous allons voir comment il exprimer le mal du siècle",
      "l-7/9":
          "question rhétorique + subordonné hypothétique -> montre qu'il est prêt à faire des sacrifices pour être auprès de Madame de Rénal + négation lexical avec la polysindète renvoit à la situationde présente de J alors qu'il est emprisonné",
      "l-8":
          "antithèse 'c'est l'absence de Madame de Rénal qui m'accable' avec un présentatif 'c'est...qui' -> met en évidence cette absence pour montrer qu'il en est victime car il est en position de sujet, -> ce qui le fait souffrir c'est l'absence de Madame de Rénal",
      "l-10/11":
          "la périphrase 'l'influence de mes contemporains' avec la métonymie 'ô XIXème siècle' -> prend une fonction politique et sociale et pousse à réfléchir sur l'influence de la société",
      "l-12/16":
          "la métaphore est introduite par des points de suspension comme s'il avait marqué un temps d'arrêt avant de la formuler\n on retrouve la métaphore filée qui désigne la vie de J et la réf à son entrée chez les De Rénal et dans la société (avec la destruction de la famille).\n Le rythme ternaire crescendo 'corps noir immense effroyable' place l'importance qu'il a prise sur la famille. Les hyperboles 'incroyable rapidité', 'bruit épouvantable' -> montrent qu'il a été facile de détruire les principes de cette famille.\nle terme péjoratif du suffixe de 'rougeatre' -> montre à la fois la passion mais aussi la violence, la rèf aux gerbes a une connotation de mort puisqu'elle peut désigner les fleurs qu'on posent sur les tombes",
      "->":
          "Il n'assume donc pas la totalité de ce qu'il éprouve et ce qu'il a fait, c'est pour cette raison qu'il s'exprime par métaphores",
      "Transition 2-3": "Sa reflection sur le mal du siècle  l'incite à  regretter ses actes",
      "l-17":
          "les aposiopèse en début de paragraphe -> traduisent le temps de reflexion de J\nla construction grammaticale atypique avec l'ellipse du verbe dans la proposition principale -> montre qu'il va à l'essentiel avec le rythme thernaire et la récurrence de mort, la difficulté de survivre dans cette société est mise en évidence par l'hyperbole",
      "l-19/23":
          "la métaphore filée de la mouche qui représente J fais réf aussi à un parasite.\n Les marqueurs temporels sont nombreux et traduisent la rapidité de l'existence avec la périphrase 'éphémère' -> Il est dans un raisonnement d'analogie",
      "?":
          "le futur de certitude -> montre qu'il accepte son sort\nl'impératif qui lui donne un ton injonctif -> montre qu'il vient de prendre pleinement conscience de ses sentiments pour Madame Rénal, la seule personne qui le motiverait pour rester en vie",
      "Conlusion":
          "à travers cet extrait, nous pouvons constater l'évolution du perso de J, à présent il a pris conscience de ses sentiments mais il n'aura plus l'occasion de les assumer, il devient la représentation du héro romantique touché par le mal du siècle",
      "Ouverture":
          "dans les chap suivant Madame de Rénal ne survit pas à la mort de J -> les sentiments étaient réciproque"
    }
  };
  final Map f1 = {
    //le coche et la mouche
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
  };
  final Map f2 = {
    //le torrent et la rivière, FINI
    "txt": "texte",
    "Analyse": {
      "INTRO":
          "les fables font partie d'un genre antique qui permet de donner une morale, ce sont des apologues. Elles relevent du registre didactique. JP est un fabuliste du 17ème avec le mouvement du classicisme. Il s'est inspiré de fabuliste antique. Les fables de LF sont les oeuvres principales de LF, c'est une oeuvre qui est publié en plusieurs fois. Le 2ème recueil est écrit en 1678 et il est composé des livres 8 à 11, dans ce recueil des humains ou des éléments naturels sont mis en place en plus des animaux, fable 23 du livre 8, hétérométrique, elle met en opposition le torrent et le rivière auquels un homme va se confronter.",
      "LECTURE": "",
      "PROB": "Comment est ce que JF montre qu'il ne faut pas se fier aux apparences ?",
      "PLAN": "1à10/description du torrent\n11à23/pérépitie de l'homme à la rivière\n14à25/moral",
      "v1-v2":
          "enjambement, altération du 'r', consonne occlusive qui insiste sur le vacarme du torrent\naspect effrayant renforcer par la répétition de 'grand' -> imposant -> image de puissance",
      "v3":
          "alexandrin composé de 2 hémistiches séparer pa un ';' montre les conséquences lié à la peur avec l'isotopie de fuir et l'accentuation 'd'horreur' -> v4 : 'trembler', 'n'osait passer'\nTout = pronom indéfini -> hyperbole, toute les espèce le fuient -> maître des lieux",
      "v5-6": "'barrière' -> torrent parait infranchissable",
      "v7-8": "pas noté",
      "v9": "négation restictive 'ne que' montre la diff entre l'apparence et la réalité",
      "v10": "montre que le torrent était en réalité innofensif",
      "Transition 1-2":
          "Dans la 1ere péripéti LF a présenté le torrent comme dangereux alors que ce n'est pas le cas, dans la 2eme partie de la fable le personnage fera face à une rivière",
      "v11": "prise de conscience du personnage (succès -> hyperbolique, ironi car au final pas dangereux)",
      "v12":
          "insiste que c'est le mm voleur, participe présent 'poursuivent tjrs' absence de temporalité pour donner effet de continuité, de qui est renforcé pour l'adj 'trjs'",
      "v13": "pas noté",
      "v14-15": "présentation de la rivière en opposition à celle du torrent isotopie de la tranquilité",
      "v16":
          "insiste sur la vision subjective de la rivière du perso, 'croire' et 'd'abord'; rime suivi -> cause conséquence -> le perso ne se méfie pas assez des apparences\nalitération fricative -> bruit calme de la rivière, les sonnorité sont aussi en antithèse avec celle du torrent",
      "v17": "'pur et nette' -> insiste sur l'apparence de facilité à la franchir",
      "v18": "présent de narration -> nous fait assister à la scène",
      "v18-19": "entrer dans la rivière protège que des voleurs",
      "v19":
          "'mais' montre qu'il va avoir d'autres dangers\n2eme hémistiches = périphrase péjorative -> casser 1ere impression de la rivière, 'onde noir' rapprochement avec 'onde menaçante' utilisé pour décrire le torrent, 'noir' représente la mort",
      "v20-23":
          "alitération en 'r' + euphénisme = mort du personnage et de son cheval\nchute du récit; anaphore de 'Tout deux' -> personne n'ai en capacité de survivre à cette rivière / isotopie de la mort = issu tragique",
      "Transition 2-3":
          "Après la rencontre de l'homme et de la rivière, le lecteur prend conscience du véritable danger ce qui débouche sur la morale",
      "v24-25":
          "morale -> présent de vérité générale = propos irréfutable, antithèse 'les gens sans bruits'/'les autres' = 2 périphrases = 2 catégories de personne -> se méfier des apparences -> se méfier des gens discret car surement hypocrite"
    }
  };
  final Map f3 = {
    //la lionne et l'ourse, FINI
    "txt": "un texte",
    "Analyse": {
      "INTRO": "Bruh",
      "LECTURE": "",
      "PROB": "En quoi la confrontation entre la lionne et l'ourse permet de dénoncer l'égocentrisme ?",
      "PLAN": "1à8/la présentation du contexte\n9à21/la confrontation Ourse/Lionne\n22à26/morale",
      "v1":
          "insiste sur importance avec la majuscule + sur sa qualité de mère\nmise en place du registre tragique avec euphénisme 'avait perdu son fils' et 'un chasseur l'avait pris' -> usotipi du deuil\n'rugissement'/'vacarme' -> isotopie bruit\nles mots vont crescendo et montre de manière hyperbolique comment la lionne exprime sont chagrin, chagrin qui a des répercussion sur tt les animaux",
      "v2-4":
          "rapport cause conséquence + rime riche 'infortuné' et 'importuns' -> l'infortune de la lionne importune les animaux\nchamps lexical nuit -> 'nuit', 'obscurité', 'sommeil', 'silence' -> nuit ne rempli pas sa fct de repos, amplification des bruits des sanglots de la lionne",
      "Transition 1-2":
          "Les plaintes incessantes de la Lionne amène à une confrontation avec l'Ourse qui va parler au nom de tt les animaux\nproximité entre la lionne et l'ourse avec -> 'ma commère' -> suppr hierarchie -> 'ma' = proximité\nourse = discour structuré -> quest rhét v9,v12 -> rappelle crime commis par la lionne",
      "v9-16": "syllogisme en 3 étapes",
      "v10":
          "à l'intérieur raisonnement par analogie -> 'Que ne vous taisez-vous aussi ?' -> Lionne devraient prendre ex sur les autres femmes qui ont vécu la mm chose et ne se sont pas plaint pour autant",
      "v9-12": "utilisation de la persuasion (faire passer l'idée par les sentiments)",
      "v14-16": "argument d'expérience -> raisonnement par analogie -> 's'il est ainsi'L'que ne vs taisez vs aussi'",
      "v9-12.": "'tous les enfant qui sont passer entre vos dents n'avaient ni père, ni mères ? -> quest rhét",
      "v13-14": "'s'il est ainsi et qu'aucun de leur mort n'ait nos tete rompus' = ref à l'ex des autres animaux",
      "v15-16":
          "'si tant de mère se sont tu, que ne vs taisez vs aussi ?' -> quest reth, conclusion logique de l'argumentation de l'ourse -> syllogisme",
      "v21": "'Hélas ! c'ets le destin quie me hait' -> arg d'autorité ref au destion = persuasion"
    }
  };
  final Map f4 = {
    //Les fées
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    "txt": "un texte",
    "Analyse": {
      "INTRO":
          "apologue = petit texte dont on doit tirer une moral, classicisme et appartient au registre merveilleux, Perrault est un écrivain du 17eme siècle (querelle -> moderne), conte des Fées = vision manichièenne (bien/mal à travers les deux soeurs)",
      "LECTURE": "",
      "PROB": "Comment l'opposition entre les 2 soeur permet une reflexion sur l'honneteté ?",
      "PLAN": "1/elem de résolution\n2/situation finale\n3/morale"
    }
  };
  @override
  _PageAffiFr createState() {
    return new _PageAffiFr();
  }
}

class _PageAffiFr extends State<PageAffiFr> {
  String whatTheme;
  Color color1 = Colors.blue;
  bool absord = false;
  bool imageExist;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Math'),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            AbsorbPointer(
              absorbing: absord,
              child: Switch(
                value: isSwitched(),
                onChanged: (value) {
                  widget.changeTheme();
                  if (value == true) {
                    whatTheme = 'D';
                  } else {
                    whatTheme = 'L';
                  }
                  absord = true;
                  Future.delayed(Duration(milliseconds: 100), () {
                    //empeche que l'utilisateur bourrine le switch et que le theme de l'image soit pas le mm que celui de l'appli
                    setState(() {
                      absord = false;
                    });
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Colors.grey[200],
                inactiveTrackColor: Colors.grey[200],
                inactiveThumbColor: Colors.grey,
              ),
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: _listWidget(),
          ),
        ));
  }

  List<Widget> _listWidget() {
    Map txt;
    if (widget.page == "P1") {
      txt = widget.p1;
    } else if (widget.page == "P2") {
      txt = widget.p2;
    } else if (widget.page == "P3") {
      txt = widget.p3;
    } else if (widget.page == "F1") {
      txt = widget.f1;
    } else if (widget.page == "F2") {
      txt = widget.f2;
    } else if (widget.page == "F3") {
      txt = widget.f3;
    } else if (widget.page == "F4") {
      txt = widget.f4;
    } else if (widget.page == "R1") {
      txt = widget.r1;
    } else if (widget.page == "R2") {
      txt = widget.r2;
    } else if (widget.page == "R3") {
      txt = widget.r3;
    }
    List<Widget> _children = [];
    _children.add(ExpansionTile(
      title: Text("Texte"),
      children: [Text(txt["txt"])],
    ));
    for (var i = 0; i < txt["Analyse"].keys.toList().length; i++) {
      String key = txt["Analyse"].keys.toList()[i].toString();
      _children.add(Column(
        //Row
        children: [
          Row(children: [
            Text(
              key + " : ",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ]),
          Text(txt["Analyse"][key].toString())
        ],
      ));
    }
    print(_children);
    return _children;
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }
}
