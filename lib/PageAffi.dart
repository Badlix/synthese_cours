import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageAffi extends StatefulWidget {
  PageAffi(this.changeTheme, this.matiereSelect, this.nomCategorie, this.page);
  final Function changeTheme;
  final String matiereSelect;
  final String nomCategorie;
  final String page;
  @override
  _PageAffi createState() {
    return new _PageAffi();
  }
}

class _PageAffi extends State<PageAffi> {
  String whatTheme;
  Color color1 = Colors.blue;
  bool absord = false;
  bool imageExist;
  @override
  Widget build(BuildContext context) {
    print("Matière = " + widget.matiereSelect);
    print("Catégorie = " + widget.nomCategorie);
    print("Page = " + widget.page);
    return Scaffold(
        appBar: AppBar(
          title: Text('Math'),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            AbsorbPointer(
              absorbing: absord,
              child: Switch(
                value: isSwitched(),
                onChanged: (value) {
                  widget.changeTheme();
                  if (value == true) {
                    whatTheme = 'D';
                  } else {
                    whatTheme = 'L';
                  }
                  absord = true;
                  Future.delayed(Duration(milliseconds: 100), () {
                    //empeche que l'utilisateur bourrine le switch et que le theme de l'image soit pas le mm que celui de l'appli
                    setState(() {
                      absord = false;
                    });
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Colors.grey[200],
                inactiveTrackColor: Colors.grey[200],
                inactiveThumbColor: Colors.grey,
              ),
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: SingleChildScrollView(
          child: InteractiveViewer(
            child: FutureBuilder<Widget>(
                future: generateImage(widget.page),
                builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
                  if (snapshot.hasData) return snapshot.data;

                  return Container(child: CircularProgressIndicator());
                }),
          ),
        ));
  }

  Future<Widget> generateImage(pages) async {
    List<Image> imageList = [];
    String pageBackup;
    if (widget.page == null) {
      pageBackup = await getStringValueSF();
    } else {
      pageBackup = widget.page;
    }
    imageList.add(await whatImage(pageBackup));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: imageList,
    );
  }

  getStringValueSF() async {
    SharedPreferences backup = await SharedPreferences.getInstance();
    String mot = backup.getString('searchBackup');
    return mot;
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }

  whatImage(var nomImage) async {
    if (whatTheme == null) {
      if (Theme.of(context).brightness == Brightness.dark) {
        whatTheme = 'D';
      } else {
        whatTheme = 'L';
      }
    }
    try {
      await rootBundle.load('assets/$nomImage$whatTheme.gif');
      //print("OK  -  assets/$nomImage$whatTheme.gif");
      imageExist = true;
    } catch (error) {
      //print("PAS OK  -  assets/$nomImage$whatTheme.gif");
      imageExist = false;
    }
    if (imageExist != null) {
      if (imageExist) {
        return Image(
          image: AssetImage('assets/$nomImage$whatTheme.gif'),
        );
      } else {
        return Image(
          image: AssetImage('assets/Bruh$whatTheme.gif'),
        );
      }
    }
  }
}
