import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math'; //num pow(num x, num exponent) = puissance

class ConvertisseurPage extends StatefulWidget {
  static const routeName = 'convertisseur';
  ConvertisseurPage(this.changeTheme);
  final Function changeTheme;
  @override
  _ConvertisseurPage createState() {
    return new _ConvertisseurPage();
  }
}

class _ConvertisseurPage extends State<ConvertisseurPage> {
  TextEditingController txtController = new TextEditingController();
  final List<List> longueur = [
    ["kilomètre (km)", 1000.0],
    ["hectomètre (hm)", 100.0],
    ["décamètre (dam)", 10.0],
    ["mètre (m)", 1.0], //unite de ref
    ["décimètre (dm)", 0.1],
    ["centimètre (cm)", 0.01],
    ["millimètre (mm)", 0.001],
    ["micromètre (µm)", pow(10, -6)],
    ["namomètre (nm)", pow(10, -9)]
  ];
  final List<List> volume = [
    ["kilolitre (kl)", 1000.0],
    ["m3", 1000.0],
    ["hectolitre (hl)", 100.0],
    ["décalitre (dal)", 10.0],
    ["litre (l)", 1.0], //unite de ref
    ["dm3", 1.0],
    ["décilitre (dl)", 0.1],
    ["centilitre (cl)", 0.01],
    ["millilitre (ml)", 0.001],
    ["cm3", 0.001],
    ["microlitre (µl)", pow(10, -6)]
  ];
  final List<List> masse = [
    ["tonne (t)", pow(10, 6)],
    ["kilogramme (kg)", 1000.0],
    ["hectogramme (hg)", 100.0],
    ["décagramme (dag)", 10.0],
    ["gramme (g)", 1.0], //unite de ref
    ["décigramme (dg)", 0.1],
    ["centigramme (cg)", 0.01],
    ["milligramme (mg)", 0.001],
    ["microgramme (µg)", pow(10, -6)],
    ["nanogramme (ng)", pow(10, -9)]
  ];
  final List<List> vitesse = [
    ["km/s", 3600.0],
    ["m/s", 3.6],
    ["km/h", 1.0], //unite de ref
    ["mm/s", 0.0036],
    ["µm/s", 3.6 * pow(10, -6)]
  ];
  final List<List> energie = [
    ["kWh", 3.6 * pow(10, 6)],
    ["mégajoule (MJ)", pow(10, 6)],
    ["kcal", 4.1867999409 * pow(10, 3)],
    ["kilojoule (kJ)", 1000.0],
    ["cal", 4.1867999409],
    ["joule (J)", 1.0], //unite de ref
    ["Ws", 1.0]
  ];
  final List<List> superficie = [
    ["km2", pow(10, 6)],
    ["hectar (ha)", pow(10, 4)],
    ["m2", 1.0], //unite de ref
    ["dm2", 0.01],
    ["cm2", pow(10, -4)],
    ["mm2", pow(10, -6)],
    ["µm2", pow(10, -12)]
  ];
  final List<List> masseVolumique = [
    ["kg/l", 1.0], //unite de ref
    ["kg/dm3", 1.0],
    ["g/cm3", 1.0],
    ["g/l", 0.001],
    ["kg/m3", 0.001],
    ["g/m3", pow(10, -6)]
  ];
  final List<List> temps = [
    ["semaine", 168.0],
    ["jour", 24.0],
    ["heure (h)", 1.0], //unite de ref
    ["minute (min)", 0.0166666667],
    ["seconde (s)", 0.0002777778],
    ["milliseconde (ms)", 2.7777777778 * pow(10, -7)],
    ["microseconde (µs)", 2, 7777777778 * pow(10, -10)]
  ];
  final List<List> frequence = [
    ["térahertz (THz)", pow(10, 12)],
    ["gigahertz (GHz)", pow(10, 9)],
    ["mégahertz (MHz)", pow(10, 6)],
    ["kilohertz (kHz)", 1000.0],
    ["hertz (Hz)", 1.0], //unite de ref
    ["millihertz (mHz)", 0.001],
    ["microhertz (µHz)", pow(10, -6)]
  ];
  var _value = "1";
  var _value2 = "1";
  int button1Value = 4;
  int button2Value = 1;
  String txt = "";
  List<List> unites = [];
  String resultat = "...";
  IconData dropDownIcon = Icons.arrow_drop_down;
  //static const int sup1 = 0x00B9;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Convertisseur'),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            Switch(
              value: isSwitched(),
              onChanged: (value) {
                widget.changeTheme();
              },
              activeTrackColor: Colors.grey,
              activeColor: Colors.grey[200],
              inactiveTrackColor: Colors.grey[200],
              inactiveThumbColor: Colors.grey,
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            ExpansionTile(
              trailing: Icon(dropDownIcon),
              title: Text('Catégories'),
              //onExpansionChanged: ,
              children: <Widget>[
                ListTile(
                  title: Text('Longueur'),
                  onTap: () {
                    unites = longueur;
                  },
                ),
                ListTile(
                  title: Text('Superficie'),
                  onTap: () {
                    unites = superficie;
                  },
                ),
                ListTile(
                  title: Text('Masse'),
                  onTap: () {
                    unites = masse;
                  },
                ),
                ListTile(
                  title: Text('Volume'),
                  onTap: () {
                    unites = volume;
                  },
                ),
                ListTile(
                  title: Text('Masse Volumique'),
                  onTap: () {
                    unites = masseVolumique;
                  },
                ),
                ListTile(
                  title: Text('Vitesse'),
                  onTap: () {
                    unites = vitesse;
                  },
                ),
                ListTile(
                  title: Text("Temps"),
                  onTap: () {
                    unites = temps;
                  },
                ),
                ListTile(
                  title: Text('Énergie'),
                  onTap: () {
                    unites = energie;
                  },
                ),
                ListTile(
                  title: Text('Fréquence'),
                  onTap: () {
                    unites = frequence;
                  },
                ),
              ],
            ),
            SizedBox(
              width: double.infinity,
              child: Card(
                color: whichColor(),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 5),
                          child: DropdownButton(
                            items: dropDownButtonItems(),
                            onChanged: (value) {
                              setState(() {
                                button1Value = int.parse(value);
                                _value = value;
                                calcul();
                              });
                            },
                            value: _value,
                          ),
                        ),
                        Spacer(flex: 1),
                        Text("->", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                        Spacer(flex: 1),
                        DropdownButton(
                          items: dropDownButtonItems(),
                          onChanged: (value2) {
                            setState(() {
                              button2Value = int.parse(value2);
                              _value2 = value2;
                              calcul();
                            });
                          },
                          value: _value2,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 120, //120 -> peut contnir pile 10 chiffres
                          height: 75,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextField(
                              enableInteractiveSelection: false,
                              keyboardType: TextInputType.phone, //ouvre le clavier avec les nombre
                              inputFormatters: [], //que des nombres peuvent être rentré
                              controller: txtController,
                              cursorColor: Colors.grey[200],
                              showCursor: true,

                              maxLength: 10,
                              decoration: InputDecoration(
                                  //border: OutlineInputBorder(borderSide: BorderSide(color: Colors.black))),
                                  border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.black))),
                              onChanged: (text) {
                                textChange();
                                calcul();
                              },
                            ),
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(right: 15, bottom: 20),
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                color: whichColorResultat(),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(right: 5, left: 5, bottom: 2, top: 2),
                                child: Text(resultat, style: TextStyle(fontSize: 20)),
                              )),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            DataTable(columnSpacing: 22, horizontalMargin: 10, columns: [
              DataColumn(label: Text("Préfixe")),
              DataColumn(label: Text("Symbole")),
              DataColumn(label: Text("Exposant")),
              DataColumn(label: Text("Nom"))
            ], rows: [
              DataRow(
                cells: [
                  DataCell(Text("yotta")),
                  DataCell(Text("Y")),
                  //DataCell(Text("10e24")),
                  DataCell(Tooltip(
                    message: "1 000 000 000 000 000 000 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e24"),
                    )),
                  )),
                  DataCell(Text("quadrillion")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("zetta")),
                  DataCell(Text("Z")),
                  //DataCell(Text("10e21")),
                  DataCell(Tooltip(
                    message: "1 000 000 000 000 000 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e21"),
                    )),
                  )),
                  DataCell(Text("trilliard")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("exa")),
                  DataCell(Text("E")),
                  //DataCell(Text("10e18")),
                  DataCell(Tooltip(
                    message: "1 000 000 000 000 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e18"),
                    )),
                  )),
                  DataCell(Text("trillion")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("péta")),
                  DataCell(Text("P")),
                  //DataCell(Text("10e15")),
                  DataCell(Tooltip(
                    message: "1 000 000 000 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e15"),
                    )),
                  )),
                  DataCell(Text("billiard")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("téra")),
                  DataCell(Text("T")),
                  //DataCell(Text("10e12")),
                  DataCell(Tooltip(
                    message: "1 000 000 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e12"),
                    )),
                  )),
                  DataCell(Text("billion")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("giga")),
                  DataCell(Text("G")),
                  //DataCell(Text("10e9")),
                  DataCell(Tooltip(
                    message: "1 000 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e9"),
                    )),
                  )),
                  DataCell(Text("milliard")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("méga")),
                  DataCell(Text("M")),
                  //DataCell(Text("10e6")),
                  DataCell(Tooltip(
                    message: "1 000 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e6"),
                    )),
                  )),
                  DataCell(Text("million")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("kilo")),
                  DataCell(Text("k")),
                  //DataCell(Text("10e3")),
                  DataCell(Tooltip(
                    message: "1 000",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e3"),
                    )),
                  )),
                  DataCell(Text("millier")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("hecto")),
                  DataCell(Text("h")),
                  //DataCell(Text("10e2")),
                  DataCell(Tooltip(
                    message: "100",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e2"),
                    )),
                  )),
                  DataCell(Text("centaine")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("déca")),
                  DataCell(Text("da")),
                  //DataCell(Text("10e1")),
                  DataCell(Tooltip(
                    message: "10",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e1"),
                    )),
                  )),
                  DataCell(Text("dizaine")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("bruh")),
                  DataCell(Text("-")),
                  //DataCell(Text("10e0")),
                  DataCell(Tooltip(
                    message: "1",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e0"),
                    )),
                  )),
                  DataCell(Text("unité")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("déci")),
                  DataCell(Text("d")),
                  //DataCell(Text("10e-1")),
                  DataCell(Tooltip(
                    message: "0.1",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-1"),
                    )),
                  )),
                  DataCell(Text("dixième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("centi")),
                  DataCell(Text("c")),
                  //DataCell(Text("10e-2")),
                  DataCell(Tooltip(
                    message: "0.01",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-2"),
                    )),
                  )),
                  DataCell(Text("centième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("milli")),
                  DataCell(Text("m")),
                  //DataCell(Text("10e-3")),
                  DataCell(Tooltip(
                    message: "0.001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-3"),
                    )),
                  )),
                  DataCell(Text("millième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("micro")),
                  DataCell(Text("µ")),
                  //DataCell(Text("10e-6")),
                  DataCell(Tooltip(
                    message: "0.000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-6"),
                    )),
                  )),
                  DataCell(Text("millionième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("nano")),
                  DataCell(Text("n")),
                  //DataCell(Text("10e-9")),
                  DataCell(Tooltip(
                    message: "0.000 000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-9"),
                    )),
                  )),
                  DataCell(Text("milliardième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("pico")),
                  DataCell(Text("p")),
                  //DataCell(Text("10e-12")),
                  DataCell(Tooltip(
                    message: "0.000 000 000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-12"),
                    )),
                  )),
                  DataCell(Text("billionième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("femto")),
                  DataCell(Text("f")),
                  //DataCell(Text("10e-15")),
                  DataCell(Tooltip(
                    message: "0.000 000 000 000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-15"),
                    )),
                  )),
                  DataCell(Text("billiardième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("atto")),
                  DataCell(Text("a")),
                  //DataCell(Text("10e-18")),
                  DataCell(Tooltip(
                    message: "0.000 000 000 000 000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-18"),
                    )),
                  )),
                  DataCell(Text("trillionième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("zepto")),
                  DataCell(Text("z")),
                  //DataCell(Text("10e-21")),
                  DataCell(Tooltip(
                    message: "0.000 000 000 000 000 000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-21"),
                    )),
                  )),
                  DataCell(Text("trilliardième")),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text("yocto")),
                  DataCell(Text("y")),
                  //DataCell(Text("10e-24")),
                  DataCell(Tooltip(
                    message: "0.000 000 000 000 000 000 000 001",
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                      child: Text("10e-24"),
                    )),
                  )),
                  DataCell(Text("quadrillionième")),
                ],
              ),
            ])
          ],
        )));
  }

  calcul() {
    String calcul2;
    print("txt = " + txt);
    print("txt.isEmpty = " + txt.isEmpty.toString());
    if (txt.isEmpty == false && unites.isEmpty == false) {
      double calcul = double.parse(txt) * (unites[button1Value][1] / unites[button2Value][1]);
      calcul2 = calcul.toStringAsPrecision(12);
      if (unites == temps) {
        calcul2 = calctemps(calcul2);
      }
      if (calcul2.toString().contains('.') == true && calcul2.contains('e') != true) {
        for (var i = 0; i < 12; i++) {
          if (calcul2.endsWith('0') == true) {
            calcul2 = calcul2.substring(0, calcul2.length - 1);
          } else if (calcul2.endsWith('.') == true) {
            calcul2 = calcul2.substring(0, calcul2.length - 1);
            break;
          }
        }
      }
      if (calcul2.contains('e') == true) {
        int indexExposant = calcul2.indexOf('e');
        String exposant = calcul2.substring(indexExposant, calcul2.length);
        calcul2 = calcul2.substring(0, indexExposant);
        for (var i = 0; i < 12; i++) {
          if (calcul2.endsWith('0') == true) {
            calcul2 = calcul2.substring(0, calcul2.length - 1);
          } else if (calcul2.endsWith('.') == true) {
            calcul2 = calcul2.substring(0, calcul2.length - 1);
            break;
          }
        }
        calcul2 = calcul2 + exposant;
      }

      setState(() {
        resultat = calcul2;
      });
    } else if (txt.isEmpty == true) {
      setState(() {
        resultat = "...";
      });
    }
  }

  calctemps(String calcul2) {
    String tempsArrondi = double.parse(calcul2).toStringAsPrecision(5);
    return tempsArrondi;
  }

  dropDownButtonItems() {
    List<DropdownMenuItem<String>> items = [];
    if (unites.isEmpty == true) {
      items.add(DropdownMenuItem(value: "1", child: Text('...')));
      AlertDialog(title: Text("Slectionner une catégorie"));
      return items;
    } else {
      for (var i = 0; i < unites.length; i++) {
        items.add(DropdownMenuItem(value: i.toString(), child: Text(unites[i][0])));
      }
      return items;
    }
  }

  whichColor() {
    return (Theme.of(context).brightness == Brightness.dark) ? Colors.grey[800] : Colors.grey[400];
  }

  whichColorResultat() {
    return (Theme.of(context).brightness == Brightness.dark) ? Colors.grey[700] : Colors.grey[500];
  }

  textChange() {
    setState(() {
      txt = txtController.text;
    });
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }

  changeExpansion(bool boolExpansion) {
    setState(() {
      if (boolExpansion == false) {
        dropDownIcon = Icons.arrow_drop_down;
      } else {
        dropDownIcon = Icons.arrow_drop_up;
      }
    });
    print(dropDownIcon);
  }
}
