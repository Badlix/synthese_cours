import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PageCodeRoute extends StatefulWidget {
  PageCodeRoute(this.changeTheme, this.page);
  final Function changeTheme;
  final String page;
  @override
  _PageCodeRoute createState() {
    return new _PageCodeRoute();
  }
}

class _PageCodeRoute extends State<PageCodeRoute> {
  String whatTheme;
  Color color1 = Colors.blue;
  bool absord = false;
  bool imageExist;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Code'),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            AbsorbPointer(
              absorbing: absord,
              child: Switch(
                value: isSwitched(),
                onChanged: (value) {
                  widget.changeTheme();
                  if (value == true) {
                    whatTheme = 'D';
                  } else {
                    whatTheme = 'L';
                  }
                  absord = true;
                  Future.delayed(Duration(milliseconds: 100), () {
                    //empeche que l'utilisateur bourrine le switch et que le theme de l'image soit pas le mm que celui de l'appli
                    setState(() {
                      absord = false;
                    });
                  });
                },
                activeTrackColor: Colors.grey,
                activeColor: Colors.grey[200],
                inactiveTrackColor: Colors.grey[200],
                inactiveThumbColor: Colors.grey,
              ),
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: SingleChildScrollView(
            child: InteractiveViewer(
          child: whichPage(),
        )));
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }

  whichPage() {
    if (widget.page == "panneau")
      return _builPanneau();
    else
      return _buildTypiquePage(widget.page);
  }

  _buildTypiquePage(page) {
    return SingleChildScrollView(
      child: Image.asset("assets/CodeRoute/image/" + page + ".png"),
    );
  }

  _builPanneau() {
    int nbImageDanger = 1; //-> éclaté mais flemme
    return SingleChildScrollView(
      child: Row(
        children: [
          Image.asset('assets/CodeRoute/image/panneaux/danger1.png'),
          ExpansionTile(
            title: Text("^"),
            children: [
              Text(
                  "1. Virage à droite\n\n2. Virage à gauche\n\n3. Succession de virages, le 1er est à droite\n\n4. Succession de virages, le 1er est à gauche\n")
            ],
          )
        ],
      ),
    );
    // List panneauDanger = [
    //   "Virage à droite/gauche",
    //   "Succession de virages, le 1er est à droite/gauche",
    //   "Dos d'âne",
    //   "Ralentisseur",
    //   "Chaussée rétrécie", //5
    //   "Chaussée rétrécie par la droite/gauche",
    //   "Chaussée particulièrement glissante",
    //   "Pont mobile",
    //   "Passage à niveau muni de barrières à fonctionnement manuel ou de demi-barrières automatiques lors du passage des trains",
    //   "Passage à niveau sans barrière, ni demi-barrière", //10
    //   "Traversée de voies de tramways",
    //   "Traversée de voies de bus",
    //   "Endroit fréquenté par les enfants",
    //   "Passage pour piéton - signal d'annonce",
    //   "Danger pour lequel il n'existe pas de symbole correspondant", //15
    //   "Passage d'animaux domestiques",
    //   "Passage d'animaux sauvages (cerfs, sangliers, renards..)",
    //   "Passage de cavaliers",
    //   "Descente dangereuse (10%, c'est descendre de 10m tous les 100m parcourus)",
    //   "Annonce de feux tricolores", //20
    //   "Circulation dans les 2 sens à partir du panneau",
    //   "Risque de chute de pierres ou présence de pierres tombées sur la route",
    //   "Débouché sur un quai ou une berge",
    //   "Débouché de cyclistes venant de DROITE ou de GAUCHE",
    //   "Traversée d'une aire de danger aérien", //25
    //   "Risque de fort vent latéral",
    //   "Carrefour à sens giratoire avec priorité aux usagers circulant sur l'anneau",
    //   "Cédez le passage aux véhicules débouchant de la ou des routes situées à sa droite",
    //   "Intersection avec une route dont les usagers doivent céder le passage (priorité temporaire pour ceux qui circulent tout droit)",
    //   "Annonce de nappes de brouillard ou de fumée épaisse", //30
    // ];
    // List panneauDangerT = [
    //   "Rétrécissement de chaussée par la droite et par la gauche",
    //   "Accident",
    //   "Projection de gravillons",
    //   "Danger temporaire pour lequel il n'existe pas de symbole correspondant (il faut alors lire les panonceaux)",
    //   "Bouchon",
    //   "Travaux",
    //   "Annonce de signaux lumineux réglant la circulation",
    //   "Dos d'âne",
    //   "Chaussée glissante",
    //   "Annonce de nappes de brouillard ou de fumée épaisses",
    //   "Signal de position d'une déviation ou d'un rétrécissement temporaire de la chaussée"
    // ];
    // List panneauInterdit = [
    //   "Circulation interdite à tous véhicules dans les 2 sens",
    //   "Sens interdit",
    //   "Interdiction de tourner à gauche/droite à la prochaine intersection",
    //   "Interdiction de faire demi-tour jusqu'à la prochaine intersection INCLUSE",
    //   "Interdiction de dépasser tous les véhicules à moteur autres que les deux-roues", //5
    //   "Interdiction de dépasser pour les transports de marchandises de plus de 3,5 tonnes",
    //   "Arrêt obligatoire au poste de douane",
    //   "Arrêt obligatoire au barrage de gendarmerie/police",
    //   "Arrêt obligatoire au poste de péage",
    //   "Accès interdit à tous les véhicules à moteur", //10
    //   "Accès interdit aux véhicules affectés au transport de marchandises, même aux camionnettes",
    //   "Accès interdit aux véhicules affectés au transport de marchandises de plus de 5,5t",
    //   "Accès interdit aux piétons",
    //   "Accès interdit aux cycles",
    //   "Accès interdit aux véhicules à moteur à l'exception des cyclomoteurs", //15
    //   "Accès interdit aux véhicules à traction animale",
    //   "Accès interdit aux véhicules agricoles à moteur",
    //   "Accès interdit aux voitures à bras à l'exclusion de celles visées à l'article R.412-34 du code la de route",
    //   "Accès interdit aux véhicules de transport en commun",
    //   "Accès interdit aux cyclomoteurs", //20
    //   "Accès interdit aux motos et motos légères",
    //   "Accès interdit aux véhicules tractant une caravane ou une remorque de plus de 250 kg",
    //   "Accès interdit aux véhicules de transport de marchandises de plus de ... m de long",
    //   "Accès interdit aux véhicules dont la largeur, chargement compris, dépasse la dimension indiquée",
    //   "Accès interdit aux véhicules dont la hauteur, chargement compris, dépasse la dimension indiquée", //25
    //   "Accès interdit aux véhicules dont le PTAC (ou le PTRA) dépasse le poids indiqué sur le panneau",
    //   "Accès interdit aux véhicules pesant sur un essieu plus de 2,5t",
    //   "Limitation de vitesse (à 50km/)",
    //   "Signaux sonores interdits",
    //   "Interdiction aux véhicules de circuler sans maintenir entre eux au moins l'intervalle indiqué", //30
    //   "Accès interdit aux véhicules transportant des marchandises explosives ou facilement inflammables",
    //   "Accès interdit aux véhicules transportant des marchandises susceptibles de polluer des eaux",
    //   "Accès interdit aux véhicules transportant des marchandises dangereuses",
    //   "Fin de toutes les interdictions précédemment signalées et imposées aux véhicules EN MOUVEMENT",
    //   "Fin de limitation de vitesse à 70 km/h", //35
    //   "Fin d'interdiction de dépasser",
    //   "Fin d'interdiction de dépasser pour les véhicules de transport de marchandises dont le PTAC est > à 3,5t",
    //   "Fin d'interdiction de l'emploi des avertisseurs sonores",
    //   "Fin d'interdiction aux skieurs",
    //   "Accès interdit aux troupeaux" //40
    // ];
    // //panneau d'interdiction relatif au stationnement
    // List panneauInterditS = [
    //   "Arrêt et stationnement interdits",
    //   "Stationnement interdit",
    //   "Stationnement interdit du côté du panneau du 1er au 15 du mois",
    //   "Stationnement interdit du côté du panneau du 16 à la fin du mois",
    //   "Stationnement interdit à gauche/droite du panneau", //5
    //   "Stationnement interdit à partir du panneau",
    //   "Stationnement interdit jusqu'au panneau",
    //   "Stationnement interdit avant et après le panneau",
    //   "Stationnement payant avec parcmètre",
    //   "Stationnement gratuit à durée limitée (zone bleue)",
    //   "Stationnement interdit aux véhicules affectés au transport de marchandises",
    //   "Stationnement gênant au sens de l'article R 417-10 du code de la route",
    //   "Stationnement interdit sauf pour les véhicules électriques",
    //   "Stationnement et arrêt interdit sauf pour l'autopartage",
    //   "Stationnement réservé aux personnes titulaires de la carte européenne",
    //   "Stationnement interdit aux remorques ou aux caravanes"
    // ];
    // //panneau d'interdiction relatif aux zones
    // List panneauInterditZ = [
    //   "Entré de zone à stationnement interdit",
    //   "Entrée de zone à stationnement unilatéral à alternance semi-mensuelle",
    //   "Entrée de zone à stationnement de durée limitée avec contrôle par disque",
    //   "Entrée de zone à stationnement payant",
    //   "Entrée de zone à stationnement unilatéral à alternance semi-mensuelle et à durée limitée contrôlée par disque",
    //   "Début de zone à 30 km/h s'appliquant sur les rues adjacentes",
    //   "Entrée de zone de circulation restreinte",
    //   // "Sortie de zone",
    //   // "Sortie de zone à stationnement unilatéral à alternance semi-mensuelle",
    //   // "Sortie de zone à stationnement de durée limitée avec contrôle par disque",
    //   // "Sortie de zone à stationnement payant",
    //   // "Sortie de zone à stationnement unilatéral à alternance semi-mensuelle et à durée limitée contrôlée par disque",
    //   // "Sortie de zone à 30 km/h s'appliquant sur les rues adjacentes",
    //   // "Sortie de zone de circulation restreinte"
    // ];
    // List panneauObligation = [
    //   "Direction obligatoire à la prochaine intersection : tout droit",
    //   "Directions obligatoires à la prochaine intersection : tout droit ou à droite",
    //   "Directions obligatoires à la prochaine intersection : tout droit ou à gauche",
    //   "Obligation de tourner à droite",
    //   "Obligation de tourner à gauche",
    //   "Obligation de contourner par la droite",
    //   "Obligation de contourner par la gauche",
    //   "Direction obligatoire à la prochaine intersection (à droite)",
    //   "Direction obligatoire à la prochaine intersection (à gauche)",
    //   "Choix entre 2 directions obligatoires (à gauche ou à droite)",
    //   "Piste ou bande cyclable réservée aux cycles à 2 ou 3 roues",
    //   "Chemin obligatoire pour les piétons",
    //   "Chemin obligatoire pour les cavaliers",
    //   "Vitesse minimale obligatoire",
    //   "Chaînes à neige obligatoires sur au moins 2 roues motrices",
    //   "Voie réservée aux véhicules des services réguliers de transport en commun",
    //   "Voie réservée aux trams",
    //   "Obligation d'allumer les feux (avant d'entrer dans un tunnel)",
    //   "Fin de piste ou bande cyclable réservée aux cycles à 2 ou 3 roues",
    //   "Fin de chemin obligatoire pour les piétons",
    //   "Fin de chemin obligatoire pour les cavaliers",
    //   "Fin de vitesse minimale obligatoire",
    //   "Chaînes à neige ne sont plus obligatoires",
    //   "Fin de voie réservée aux véhicules des services réguliers de transport en commun",
    //   "Fin de voie réservée aux trams",
    //   "Fin d'obligation selon inscription"
    // ];
    // //finir tt les cours sur les panneaux + arriver au cour où y'a tt les panneaux
    // return Column(
    //   children: [
    //     Row(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [SizedBox(width: MediaQuery.of(context).size.width, child: Image.asset('assets/panneauP1.png'))],
    //     ),
    //     ExpansionTile(title: Text("Liste Panneaux triangle :"), children: [
    //       Table(
    //           border: TableBorder.all(), //les images ne sont pas centrés
    //           children: List.generate(panneauInterditZ.length, (row) {
    //             return TableRow(
    //                 children: List.generate(2, (column) {
    //               if (column == 0) {
    //                 String nameImage = "interditZ" + (row + 1).toString() + ".png";
    //                 double taille = 50;
    //                 return SizedBox(height: taille, child: Image.asset('assets/panneaux/$nameImage'));
    //               } else {
    //                 return Padding(
    //                   padding: const EdgeInsets.all(8.0),
    //                   child: Text(
    //                     panneauDanger[row],
    //                     textAlign: TextAlign.center,
    //                   ),
    //                 );
    //               }
    //             }));
    //           })),
    //     ]),
    //     ExpansionTile(title: Text("Liste Panneaux triangle jaune :"), children: [
    //       Table(
    //           border: TableBorder.all(), //les images ne sont pas centrés
    //           children: List.generate(panneauDangerT.length, (row) {
    //             return TableRow(
    //                 children: List.generate(2, (column) {
    //               if (column == 0) {
    //                 String nameImage = "dangert" + (row + 1).toString() + ".png";
    //                 return Container(
    //                     alignment: Alignment.center,
    //                     child: SizedBox(height: 40, child: Image.asset('assets/panneaux/$nameImage')));
    //               } else {
    //                 return Padding(
    //                   padding: const EdgeInsets.all(8.0),
    //                   child: Text(
    //                     panneauDangerT[row],
    //                     textAlign: TextAlign.center,
    //                   ),
    //                 );
    //               }
    //             }));
    //           })),
    //     ]),
    //     Row(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [
    //         SizedBox(width: MediaQuery.of(context).size.width, child: Image.asset("assets/panneauP2.png")),
    //       ],
    //     ),
    //     Row(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [
    //         SizedBox(width: MediaQuery.of(context).size.width, child: Image.asset("assets/panneauP3.png")),
    //       ],
    //     ),
    //     Row(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [
    //         SizedBox(width: MediaQuery.of(context).size.width, child: Image.asset("assets/panneauP4.png")),
    //       ],
    //     ),
    //     Row(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [
    //         SizedBox(width: MediaQuery.of(context).size.width, child: Image.asset("assets/panneauP5.png")),
    //       ],
    //     ),
    // Row(
    //   mainAxisAlignment: MainAxisAlignment.start,
    //   children: [
    //     Image.asset("assets/panneauP6.png"),
    //   ],
    // ),
    // Row(
    //   mainAxisAlignment: MainAxisAlignment.start,
    //   children: [
    //     Image.asset("assets/panneauP7.png"),
    //   ],
    // ),
    // Row(
    //   mainAxisAlignment: MainAxisAlignment.start,
    //   children: [
    //     Image.asset("assets/panneauP8.png"),
    //   ],
    // )
    //   ],
    // );
  }
}
