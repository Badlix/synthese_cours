import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synthese_cours/PageAffiFr.dart';
import 'package:synthese_cours/codeRoute.dart';
import 'package:synthese_cours/testImage.dart';
import 'package:path/path.dart' as path;

import 'PageAffi.dart';
import 'convert.dart';
import 'convertBinaire.dart';
import 'search.dart';

void main() async {
  // Permet de lancer l'application en mode desktop sur Linux
  // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override
  // BLE : pratique pour tester sans lancer d'émulateur
  WidgetsFlutterBinding.ensureInitialized();
  String fichier = await rootBundle.loadString('assets/fichier.json');
  Map mapFichier = json.decode(fichier);
  //debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  runApp(MyApp(mapFichier));
}

class MyApp extends StatefulWidget {
  MyApp(this.mapFichier);
  final Map mapFichier;
  @override
  _MyApp createState() {
    return new _MyApp();
  }
}

class _MyApp extends State<MyApp> {
  String theme = 'light';
  String matiereSelect;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My courses',
      theme: _buildThemeData(),
      initialRoute: HomePage.routeName,
      routes: <String, WidgetBuilder>{
        HomePage.routeName: (BuildContext context) => HomePage(changeTheme, widget.mapFichier),
        PageMenu.routeName: (BuildContext context) => PageMenu(changeTheme, widget.mapFichier, matiereSelect),
        ConvertisseurPage.routeName: (BuildContext context) => ConvertisseurPage(changeTheme),
        ConvertBinaire.routeName: (BuildContext context) => ConvertBinaire(changeTheme),
      },
    );
  }

  changeTheme() {
    setState(() {
      if (theme == 'light') {
        theme = 'dark';
      } else {
        theme = 'light';
      }
    });
  }

  ThemeData _buildThemeData() {
    return ThemeData(
        brightness: whatTheme(theme),
        fontFamily: 'Roboto',
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          TargetPlatform.fuchsia: CupertinoPageTransitionsBuilder()
        }));
  }

  whatTheme(String darkOrLight) {
    sleep(const Duration(milliseconds: 200)); //pour que les images changent en mm temps que l'arrière plan
    return (darkOrLight == 'light') ? Brightness.light : Brightness.dark;
  }
}

class HomePage extends StatefulWidget {
  static const routeName = 'home';
  HomePage(this.changeTheme, this.mapFichier);
  final Function changeTheme;
  final Map mapFichier;

  @override
  _HomePage createState() {
    return new _HomePage();
  }
}

class _HomePage extends State<HomePage> {
  //page d'accueil avec les matières
  bool chargerListPageJson = false;
  List<String> allPage = [];
  String matiereSelect;
  String page;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(children: [
          Text('Synthèse Cours'),
          Padding(
            padding: const EdgeInsets.only(left: 5.0),
            child: IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchPage(widget.changeTheme, widget.mapFichier)));
              },
            ),
          )
        ]),
        actions: <Widget>[
          Icon(Icons.wb_sunny),
          Switch(
            value: isSwitched(),
            onChanged: (value) {
              widget.changeTheme();
            },
            activeTrackColor: Colors.grey,
            activeColor: Colors.grey[200],
            inactiveTrackColor: Colors.grey[200],
            inactiveThumbColor: Colors.grey,
          ),
          Icon(Icons.brightness_3)
        ],
      ),
      body: Column(children: [
        //createMenu(),
        ListTile(
          title: Text("Français"),
          onTap: () {
            matiereSelect = "Fr";
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageMenu(widget.changeTheme, widget.mapFichier, matiereSelect)));
          },
        ),
        ListTile(
          title: Text("Math"),
          onTap: () {
            matiereSelect = "Math";
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageMenu(widget.changeTheme, widget.mapFichier, matiereSelect)));
          },
        ),
        ListTile(
          title: Text("Physique"),
          onTap: () {
            matiereSelect = "Physique";
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageMenu(widget.changeTheme, widget.mapFichier, matiereSelect)));
          },
        ),
        ListTile(
          title: Text('Convertisseur'),
          onTap: () {
            Navigator.pushNamed(context, ConvertisseurPage.routeName);
          },
        ),
        ListTile(
          title: Text("Rubik's cube"),
          onTap: () {
            page = "rubiksCube";
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => PageAffi(widget.changeTheme, "", "", page)));
          },
        ),
        ListTile(
          title: Text("Convertisseur Binaire"),
          onTap: () {
            Navigator.pushNamed(context, ConvertBinaire.routeName);
          },
        ),
        ListTile(
            title: Text("Code de la Route"),
            onTap: () {
              matiereSelect = "CodeRoute";
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PageMenu(widget.changeTheme, widget.mapFichier, matiereSelect)));
            }),
        ListTile(
          title: Text("Test Image 2"),
          onTap: () {
            page = "test1.json";
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => TestImageAffi(widget.changeTheme, widget.mapFichier, page)));
          },
        ),

        Spacer(flex: 1),
        Row(
          children: [
            FlatButton(
              onPressed: () {
                // createGIFfromODGfiles();
              },
              child: Text("Create gif from odg Files"),
              // color: Colors.red,
              color: Colors.red[200],
            ),
          ],
        ),
        Row(
          children: [
            FlatButton(
              onPressed: () {
                for (var i = 0; i < widget.mapFichier.keys.toList().length; i++) {
                  String matiere = widget.mapFichier.keys.toList()[i];
                  for (var j = 0; j < widget.mapFichier[matiere].keys.toList().length; j++) {
                    String categorie = widget.mapFichier[matiere].keys.toList()[j];
                    for (var k = 0; k < widget.mapFichier[matiere][categorie].values.toList().length; k++) {
                      allPage.add(widget.mapFichier[matiere][categorie].values.toList()[k]);
                    }
                  }
                }
                allPage.shuffle();
                addListToSF(allPage);
              },
              child: Text("Update liste page random"),
              color: Colors.red,
            ),
          ],
        ),
        // Spacer(flex: 0.2),
        Row(
          children: [
            FlatButton(
                color: Colors.red,
                onPressed: () {
                  checkLesAssetsQuiManque();
                },
                child: Text("Assets manquant")),
            Spacer(flex: 1),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton(
                //error -> future dynamic not subtype of string
                onPressed: () {
                  // if (chargerListPageJson == false) {
                  //   chargerListPageJson = true;
                  //   for (var i = 0; i < widget.mapFichier.keys.toList().length; i++) {
                  //     String matiere = widget.mapFichier.keys.toList()[i];
                  //     for (var j = 0; j < widget.mapFichier[matiere].keys.toList().length; j++) {
                  //       String categorie = widget.mapFichier[matiere].keys.toList()[j];
                  //       for (var k = 0; k < widget.mapFichier[matiere][categorie].values.toList().length; k++) {
                  //         allPage.add(widget.mapFichier[matiere][categorie].values.toList()[k]);
                  //       }
                  //     }
                  //   }
                  //   print(allPage);
                  // }
                  // allPage.shuffle();
                  // addListToSF(allPage);
                  // Random random = Random();
                  // int numRandom = random.nextInt(allPage.length);
                  // String pageRandom = allPage[numRandom];
                  // page = pageRandom;
                  page = getStringValueSF();
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => PageAffi(widget.changeTheme, "", "", page)));
                },
                child: Text(
                  "?",
                  style: TextStyle(fontSize: 35),
                ),
              ),
            )
          ],
        )
      ]),
    );
  }

  getStringValueSF() async {
    SharedPreferences instanceSF = await SharedPreferences.getInstance();
    List listPageRandom = instanceSF.getStringList('listPageRandom');
    String pageRandom = listPageRandom[0];
    // print("page random = $pageRandom");
    listPageRandom.removeAt(0);
    // print("newList = $listPageRandom");
    addListToSF(listPageRandom);
    return pageRandom;
  }

  addListToSF(liste) async {
    //SF = SharedPeference
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{}; // set initial values here if desired
      }
      return null;
    });
    SharedPreferences backup = await SharedPreferences.getInstance();
    backup.setStringList('listPageRandom', liste);
    // print(liste);
  }

  createGIFfromODGfiles() {
    var dossierFini = Directory("recap-original/fini"); //recup la list de tout les fichier odg
    var dossierAFaire = Directory("recap-original/D à faire");
    List<String> allODGFiles = [];
    List listFilesDossierFini = dossierFini.listSync();
    List listFilesDossierAFaire = dossierAFaire.listSync();
    List listAllFiles = listFilesDossierFini + listFilesDossierAFaire;
    for (var i = 0; i < listAllFiles.length; i++) {
      String nomFile = path.basename(listAllFiles[i].path).toString();
      allODGFiles.add(nomFile.substring(0, nomFile.indexOf('.')));
    }
    allODGFiles.sort();
    var dossierAsset = Directory("assets"); //recup la list de tt les gif
    List<String> allGIF = [];
    List listGIFDossierAsset = dossierAsset.listSync();
    for (var i = 0; i < listGIFDossierAsset.length; i++) {
      if (path.basename(listGIFDossierAsset[i].path.toString()).contains(".gif") == true) {
        String nomGIF = path.basename(listGIFDossierAsset[i].path).toString();
        allGIF.add(nomGIF.substring(0, nomGIF.indexOf('.')));
      }
    }
    allGIF.sort();
    List odgAConvertirEnGif = [];
    for (var i = 0; i < allODGFiles.length; i++) {
      if (allGIF.contains(allODGFiles[i]) == false) {
        odgAConvertirEnGif.add(allODGFiles[i]);
      }
    }
    print("");
    print("A CONVERT :");
    print("");
    print(odgAConvertirEnGif);
  }

  checkLesAssetsQuiManque() async {
    List<String> pageManquante = [];
    List matiere = widget.mapFichier.keys.toList();
    List chapitre;
    List nomPage;
    String imageATester;
    for (var i = 0; i < matiere.length; i++) {
      chapitre = widget.mapFichier[matiere[i]].keys.toList();
      for (var j = 0; j < chapitre.length; j++) {
        nomPage = widget.mapFichier[matiere[i]][chapitre[j]].values.toList();
        for (var k = 0; k < nomPage.length; k++) {
          imageATester = nomPage[k] + "L";
          try {
            await rootBundle.load('assets/$imageATester.gif');
            //print("Ok - assets/$imageATester.gif");
          } catch (error) {
            //print("PAS Ok - assets/$imageATester.gif");
            pageManquante.add("$imageATester");
          }
          imageATester = nomPage[k] + "D";
          try {
            await rootBundle.load('assets/$imageATester.gif');
            //print("Ok - assets/$imageATester.gif");
          } catch (error) {
            //print("PAS Ok - assets/$imageATester.gif");
            pageManquante.add("$imageATester");
          }
        }
      }
    }
    print("Pages inexistante : " + pageManquante.toString());
  }

  // createMenu() {
  //   List<ListTile> _children = [];
  //   for (var i = 0; i < widget.mapFichier.keys.toList().length; i++) {
  //     _children.add(
  //       ListTile(
  //         title: Text(widget.mapFichier.keys.toList()[i]),
  //         onTap: () {
  //           String matiereSelect = widget.mapFichier.keys.toList()[i];
  //           Navigator.push(context, MaterialPageRoute(builder: (context) => PageMenu(widget.changeTheme, widget.mapFichier, matiereSelect)));
  //         },
  //       ),
  //     );
  //   }
  //   print(_children);
  //   _children.add(ListTile(
  //     title: Text('Convertisseur'),
  //     onTap: () {
  //       Navigator.pushNamed(context, ConvertisseurPage.routeName);
  //     },
  //   ));
  //   return _children;
  // }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }
}

class PageMenu extends StatefulWidget {
  //page de menu d'une matière avec les ExpansionTiles
  static const routeName = 'pageMenu';
  PageMenu(this.changeTheme, this.mapFichier, this.matiereSelect);
  final Function changeTheme;
  final Map mapFichier;
  final String matiereSelect;
  @override
  _PageMenu createState() {
    return new _PageMenu();
  }
}

class _PageMenu extends State<PageMenu> {
  String page;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.matiereSelect),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            Switch(
              value: isSwitched(),
              onChanged: (value) {
                widget.changeTheme();
              },
              activeTrackColor: Colors.grey,
              activeColor: Colors.grey[200],
              inactiveTrackColor: Colors.grey[200],
              inactiveThumbColor: Colors.grey,
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: SingleChildScrollView(
          child: Column(children: createExpansionTile()),
        ));
  }

  createExpansionTile() {
    if (widget.matiereSelect == "Fr") {
      List<Widget> _children1 = [];
      for (var i = 0; i < widget.mapFichier[widget.matiereSelect].keys.toList().length; i++) {
        _children1.add(
          ExpansionTile(
            title: Text(widget.mapFichier[widget.matiereSelect].keys.toList()[i]),
            children: createListTileFr(widget.mapFichier[widget.matiereSelect].keys.toList()[i]),
          ),
        );
      }
      return _children1;
    } else if (widget.matiereSelect == "CodeRoute") {
      List<Widget> _children1 = [];
      for (var i = 0; i < widget.mapFichier[widget.matiereSelect].keys.toList().length; i++) {
        _children1.add(
          ExpansionTile(
            title: Text(widget.mapFichier[widget.matiereSelect].keys.toList()[i]),
            children: createListTileCodeRoute(widget.mapFichier[widget.matiereSelect].keys.toList()[i]),
          ),
        );
      }
      return _children1;
    } else {
      List<Widget> _children1 = [];
      for (var i = 0; i < widget.mapFichier[widget.matiereSelect].keys.toList().length; i++) {
        _children1.add(
          ExpansionTile(
            title: Text(widget.mapFichier[widget.matiereSelect].keys.toList()[i]),
            children: createListTile(widget.mapFichier[widget.matiereSelect].keys.toList()[i]),
          ),
        );
      }
      return _children1;
    }
  }

  createListTileFr(String nomCategorie) {
    List<ListTile> _children2 = [];
    for (var i = 0; i < widget.mapFichier[widget.matiereSelect][nomCategorie].keys.toList().length; i++) {
      _children2.add(
        ListTile(
          title: Text(widget.mapFichier[widget.matiereSelect][nomCategorie].keys.toList()[i]),
          onTap: () {
            page = widget.mapFichier[widget.matiereSelect][nomCategorie].values.toList()[i];
            print(page);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageAffiFr(widget.changeTheme, widget.matiereSelect, nomCategorie, page)));
          },
        ),
      );
    }
    // print("Categorie = " + nomCategorie);
    return _children2;
  }

  createListTile(String nomCategorie) {
    List<ListTile> _children2 = [];
    for (var i = 0; i < widget.mapFichier[widget.matiereSelect][nomCategorie].keys.toList().length; i++) {
      _children2.add(
        ListTile(
          title: Text(widget.mapFichier[widget.matiereSelect][nomCategorie].keys.toList()[i]),
          onTap: () {
            page = widget.mapFichier[widget.matiereSelect][nomCategorie].values.toList()[i];
            print(page);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageAffi(widget.changeTheme, widget.matiereSelect, nomCategorie, page)));
          },
        ),
      );
    }
    // print("Categorie = " + nomCategorie);
    return _children2;
  }

  createListTileCodeRoute(String nomCategorie) {
    List<ListTile> _children2 = [];
    for (var i = 0; i < widget.mapFichier[widget.matiereSelect][nomCategorie].keys.toList().length; i++) {
      _children2.add(
        ListTile(
          title: Text(widget.mapFichier[widget.matiereSelect][nomCategorie].keys.toList()[i]),
          onTap: () {
            page = widget.mapFichier[widget.matiereSelect][nomCategorie].values.toList()[i];
            print(page);
            Navigator.push(context, MaterialPageRoute(builder: (context) => PageCodeRoute(widget.changeTheme, page)));
          },
        ),
      );
    }
    // print("Categorie = " + nomCategorie);
    return _children2;
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }
}
