import os
import time

# odg spécifications:
# couleur fond = #efefef
# flèche épaisseur > 0,03

# name = "ions"
# nomFichier = name + "L.odg"

fichiersODG = []
for files in os.walk('../recap-original/convert'):
    for filename in files:
        fichiersODG.append(filename)

# prend juste le nom des fichiers (et pas le path )
fichiersODG = fichiersODG[2]
os.chdir("../recap-original/convert")

for i in range(len(fichiersODG)):
    nomFichier = fichiersODG[i][:-4]
    # mode L
    os.system("soffice --convert-to pdf " + nomFichier + ".odg")
    os.system("convert -density 100x100 -units pixelsperinch " +
              nomFichier+'.pdf  -quality 100 ' + nomFichier+"L.png")
    os.system("mv " + nomFichier + "L.png ../../assets/")
    os.system("rm " + nomFichier + ".pdf")
    # mode D
    os.system("mkdir unzipODG")
    os.system("mv " + nomFichier + ".odg unzipODG")
    os.chdir("unzipODG")
    os.system("unzip " + nomFichier + ".odg")
    test = open("content.xml", "r")
    txt = test.read()
    test.close()
    test = open("content.xml", "w")
    for k in txt:
        txt = txt.replace("#efefef", "#303030")
    test.write(txt)
    test.close()
    os.system("mv " + nomFichier + ".odg ../")
    os.system("zip -r " + nomFichier + "D.odg .")
    os.system("soffice --convert-to pdf " + nomFichier + "D.odg")
    os.system("convert -density 300x300 -units pixelsperinch " + nomFichier +
              'D.pdf -fill "#fd1b1b" -opaque "#ff0000" -fill "#03a9f4" -opaque "#0000ff" -fill "#1bad05" -opaque "#008000" -fill "#303030" -opaque "#efefef" -fill "#ffffff" -opaque "#000000" -fill "#43c330" -opaque "#18a303" -quality 100 ' + nomFichier+"D.png")
    os.system("mv " + nomFichier + "D.png ../../../assets/")
    os.chdir('../')
    os.system("rm -r unzipODG")
    # essentiel
    os.system("mv " + nomFichier + ".odg ../ODG/")


# for i in range(len(fichiersODG)):
#     nomFichier = fichiersODG[i][:-4]
#     os.system("soffice --convert-to pdf " + nomFichier + ".odg")
#     os.system("convert -density 100x100 -units pixelsperinch " +
#               nomFichier+'.pdf  -quality 100 ' + nomFichier+"L.png")
#     os.system("convert -density 300x300 -units pixelsperinch " +
#               nomFichier+'.pdf -fill "#303030" -opaque "#efefef" -fill "#ffffff" -opaque "#000000" -fill "#43c330" -opaque "#18a303" -quality 100 ' + nomFichier+"D.png")
#     os.system("mv " + nomFichier + "L.png ../asset")
#     os.system("mv " + nomFichier + "D.png ../asset")
#     os.system("rm " + nomFichier + ".pdf")
#     os.system("mv " + nomFichier + ".odg ../ODG")


# MÉTHODE OU ON CHANGE L'ODG PUIS ON LE CONVERTI EN GIF

# for i in range(len(fichiersODG)*2):
#     if i % 2 == 0:
#         nomFichier = fichiersODG[int(i/2)][:-4]
#         mode = "L"
#     else:
#         mode = "D"
#     os.system("mkdir unzipODG")
#     os.system("mv " + nomFichier + ".odg unzipODG")
#     os.chdir("unzipODG")
#     os.system("unzip " + nomFichier + ".odg")
#     if i % 2 != 0:
#         # modif D
#         test = open("content.xml", "r")
#         txt = test.read()
#         test.close()
#         test = open("content.xml", "w")
#         for k in txt:
#             txt = txt.replace("#18a303", "#43c330")
#             txt = txt.replace("#efefef", "#303030")
#             txt = txt.replace("#000000", "#ffffff")
#             txt = txt.replace("black", "white")
#         test.write(txt)
#         test.close()
#         test = open("styles.xml", "r")
#         txt = test.read()
#         test.close()
#         test = open("styles.xml", "w")
#         for k in txt:
#             txt = txt.replace("#18a303", "#43c330")
#             txt = txt.replace("#efefef", "#303030")
#             txt = txt.replace("#000000", "#ffffff")
#             txt = txt.replace("black", "white")
#         test.write(txt)
#         test.close()
#     # else:
#         # modifie xml L
#         # test = open("content.xml", "r")
#         # txt = test.read()
#         # test.close()
#         # test = open("content.xml", "w")
#         # for j in txt:
#         #     txt = txt.replace("#18a303", "#43c330")
#         # test.write(txt)
#         # test.close()
#     os.system("mv " + nomFichier + ".odg ../")
#     # time.sleep(10)
#     os.system("zip -r " + nomFichier + mode + ".odg .")
#     os.system("soffice --headless --invisible --convert-to gif " +
#               nomFichier + mode + ".odg .")
#     os.system("mv " + nomFichier + mode + ".gif ../../asset/")

#     os.chdir('../')
#     os.system("rm -r unzipODG")
#     if i % 2 != 0:
#         os.system("mv " + nomFichier + ".odg ../ODG/")

# # os.remove(".*") # enlève fichier cachés
