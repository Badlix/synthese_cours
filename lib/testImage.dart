import 'dart:ui';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async' show Future;
import 'maps/test1.dart';
// import 'package:catex/catex.dart';

class TestImageAffi extends StatefulWidget {
  TestImageAffi(this.changeTheme, this.mapFichier, this.page);
  final Function changeTheme;
  final Map mapFichier;
  final String page;
  @override
  _TestImageAffi createState() {
    return new _TestImageAffi();
  }
}

class _TestImageAffi extends State<TestImageAffi> {
  String whatTheme;
  Color couleurPoliceTheme;
  bool absord = false;
  double fontSizeTitle = 45;
  Map jsonResult;
  // loadJson(nomJson) async {
  //   String data = await rootBundle.loadString('assets/' + nomJson);
  //   // jsonResult = json.decode(data);
  //   // print(jsonResult);
  //   return data;
  // }

  @override
  // void initState() {
  //   //A mettre qd ça marchera pour que le fichier se charge qu'une fois
  //   super.initState();
  //   WidgetsBinding.instance.addPostFrameCallback((_) async {
  //     await loadJson(widget.page);
  //   });
  // }

  Widget build(BuildContext context) {
    if (isSwitched() == true) {
      couleurPoliceTheme = Colors.white;
    } else {
      couleurPoliceTheme = Colors.black;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Math'),
        actions: <Widget>[
          Icon(Icons.wb_sunny),
          AbsorbPointer(
            absorbing: absord,
            child: Switch(
              value: isSwitched(),
              onChanged: (value) {
                widget.changeTheme();
                if (value == true) {
                  whatTheme = 'D';
                  couleurPoliceTheme = Colors.white;
                } else {
                  whatTheme = 'L';
                  couleurPoliceTheme = Colors.black;
                }
                absord = true;
                Future.delayed(Duration(milliseconds: 100), () {
                  //empeche que l'utilisateur bourrine le switch et que le theme de l'image soit pas le mm que celui de l'appli
                  setState(() {
                    absord = false;
                  });
                });
              },
              activeTrackColor: Colors.grey,
              activeColor: Colors.grey[200],
              inactiveTrackColor: Colors.grey[200],
              inactiveThumbColor: Colors.grey,
            ),
          ),
          Icon(Icons.brightness_3)
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [Text(testmap["titre"]), Text(testmap.keys.toString())]),
          // FutureBuilder(
          //     future: decoupeText(),
          //     builder: (BuildContext context, AsyncSnapshot<Widget> snapshot) {
          //       if (snapshot.hasData) return snapshot.data;

          //       return Container(child: CircularProgressIndicator());
          //     })
        ],
        // children: [
        //   Spacer(flex: 1),
        //   Wrap(
        //     spacing: 4.0, // gap between adjacent chips
        //     runSpacing: 4.0, // gap between lines
        //     direction: Axis.horizontal,
        //     children: [
        //       Text("Heyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"),
        //       Text("Slttttttt tttttttttttttt"),
        //       Text("BONJOURRRRRRRRRRRRRR"),
        //       // Text(jsonResult['Titre'].toString()),
        //       Math.tex(
        //         r'\color{firebrick} \overleftharpoon{ac}',
        //         textStyle: TextStyle(fontSize: 42),
        //         // logicalPpi: MathOptions.defaultLogicalPpiFor(42),
        //       ),
        //     ],
        //   ),
        //   Spacer(flex: 1)
        // ],
        // children: [
        // Row(
        //   children: [
        //     titre("TITRE", "(en t)"),
        //   ],
        // ),
        // Text("\n"),
        // def("Bonjour", "formule de politesse pour saluer quelqu'un"),
        // exemple("voici un exemple pertinent egeeeeeeeeeee dsgvebztgr tr hrt hrt fzez frg ef z"),
        // sousTitre("1) Ceci est un soustitre"),
        // loi("Gravité", "les corps plus léger sont attiré par un corps plus gros"),
        // Row(
        //   children: [
        //     RichText(
        //       text: TextSpan(text: 'U', style: TextStyle(fontSize: 30, color: Colors.black), children: [
        //         indice("(n+1)", 30),
        //       ]),
        //     ),
        //   ],
        // ),
        // DefaultTextStyle.merge(
        //   style: TextStyle(
        //     fontSize: 42,
        //   ),
        //   child: CaTeX(r'\sf{U_{(\textcolor{blue}{n}+1)}}'),
        // ),
        // Text(
        //   String.fromCharCode($radic) + "_",
        //   style: TextStyle(fontSize: 32),
        // ),
        // Text(
        //   widget.mapFichier["Math"]["Nombre"]["Test"]["Descri"].toString(),
        // ),
        // ],
      ),
    );
  }

  loadJson() async {
    String data = await rootBundle.loadString('assets/fichier.json');
    Map<String, dynamic> jsonResult = json.decode(data);
    return jsonResult;
  }

  Future<Widget> decoupeText() async {
    Map<String, dynamic> json = await loadJson();
    List clefs = ["Titre", "Txt1"];
    List<int> index = [0, 0];
    String spec;
    List<Widget> listChildren = [];
    List listTexT = []; //liste des différentes partie d'un txt avec les spécifications
    for (var key in clefs) {
      for (var element in widget.mapFichier["Math"]["Nombre"]["Test"][key]) {
        index = [0, 0];
        for (var i = 0; i < element.length; i++) {
          if (element[i] == "@") {
            if (i != 0) {
              index[1] = i;
              listTexT.add([]);
              listTexT[listTexT.length - 1].add(element.substring(index[0], index[1]));
            }
            index[0] = i + 1;
          }
          if (element[i] == "}" && i != element.length - 1) {
            if (element[i + 1] == "-") {
              index[1] = i;
              listTexT.add([]);
              listTexT[listTexT.length - 1].add(element.substring(index[0], index[1]));
              listTexT[listTexT.length - 1].add(spec);
              index[0] = i + 2;
            }
          }
          if (element[i] == "-" && i != element.length - 1) {
            if (element[i + 1] == "{") {
              index[1] = i;
              spec = element.substring(index[0], index[1]);
              index[0] = i + 2;
            }
          }
          if (i == element.length - 1) {
            if (element[i] != "-" && element[i - 1] != "}") {
              listTexT.add([]);
              listTexT[listTexT.length - 1].add(element.substring(index[0], i));
            }
          }
          // print(listTexT);
        }
        listChildren.addAll(txtGenerate(listTexT, key));
        listTexT = [];
      }
    }
    return Row(
      children: listChildren,
    );
  }

  txtGenerate(listText, String clef) {
    List<Widget> listChildren = [];
    for (var i = 0; i < listText.length; i++) {
      if (listText[i].length == 1) {
        listChildren.add(Text("bouh"));
      }
    }
  }

  WidgetSpan exposant(String txt, int fontsize) {
    if (txt == "_") {
      return WidgetSpan(
        child: Transform.translate(
          offset: Offset(2, -(fontsize * (2 / 3))),
          child: Text(
            txt,
            //superscript is usually smaller in size
            // textScaleFactor: 0.7,
            style: TextStyle(color: Colors.black, fontSize: fontsize * (22 / 30), fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else if (txt == '+') {
      return WidgetSpan(
        child: Transform.translate(
          offset: Offset(2, -(fontsize / 2)),
          child: Text(
            txt,
            //superscript is usually smaller in size
            // textScaleFactor: 0.7,
            style: TextStyle(color: Colors.black, fontSize: fontsize * (22 / 30), fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else {
      return WidgetSpan(
        child: Transform.translate(
          offset: Offset(2, -(fontsize / 2)),
          child: Text(
            txt,
            //superscript is usually smaller in size
            // textScaleFactor: 0.7,
            style: TextStyle(color: Colors.black, fontSize: fontsize * (19 / 30), fontWeight: FontWeight.bold),
          ),
        ),
      );
    }
  }

  WidgetSpan indice(String txt, double fontsize) {
    if (txt == "_") {
      return WidgetSpan(
        child: Transform.translate(
          offset: Offset(2, 1 / fontsize - fontsize / 5),
          child: Text(
            txt,
            //superscript is usually smaller in size
            // textScaleFactor: 0.7,
            style: TextStyle(color: Colors.black, fontSize: fontsize * (22 / 30), fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else if (txt == '+') {
      return WidgetSpan(
        child: Transform.translate(
          offset: Offset(2, fontsize / 10),
          child: Text(
            txt,
            //superscript is usually smaller in size
            // textScaleFactor: 0.7,
            style: TextStyle(color: Colors.black, fontSize: fontsize * (22 / 30), fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else {
      return WidgetSpan(
        child: Transform.translate(
          offset: Offset(2, fontsize / 10),
          child: Text(
            txt,
            //superscript is usually smaller in size
            // textScaleFactor: 0.7,
            style: TextStyle(color: Colors.black, fontSize: fontsize * (19 / 30), fontWeight: FontWeight.bold),
          ),
        ),
      );
    }
  }

  // Widget titre(String txt, String note) {
  //   if (note != "") {
  //     return Text(
  //       txt,
  //       style: TextStyle(fontSize: fontSizeTitle, fontWeight: FontWeight.bold),
  //     );
  //   } else {
  //     return RichText(
  //       text: TextSpan(text: txt, style: TextStyle(fontSize: fontSizeTitle, color: Colors.black), children: <TextSpan>[
  //         TextSpan(text: "  " + note, style: TextStyle(fontSize: fontSizeTitle / 2, fontWeight: FontWeight.normal)),
  //       ]),
  //     );
  //   }
  // }

  // Widget sousTitre(txt) {
  //   return Container(
  //     decoration: BoxDecoration(
  //         border: Border(
  //             bottom: BorderSide(
  //       color: Colors.blue,
  //       width: 4.0,
  //     ))),
  //     child: Text(
  //       txt,
  //       style: TextStyle(
  //         fontWeight: FontWeight.bold,
  //         color: Colors.blue,
  //         fontSize: 35,
  //       ),
  //     ),
  //   );
  // }

  // Widget def(txt1, txt2) {
  //   return RichText(
  //     text: TextSpan(
  //         text: txt1,
  //         style: TextStyle(
  //           color: Colors.green,
  //           fontWeight: FontWeight.bold,
  //           fontSize: 30,
  //         ),
  //         children: <TextSpan>[
  //           TextSpan(text: " : " + txt2, style: TextStyle(fontWeight: FontWeight.normal, fontSize: 25))
  //         ]),
  //   );
  // }

  // Widget loi(txt1, txt2) {
  //   return RichText(
  //     text: TextSpan(
  //         text: txt1,
  //         style: TextStyle(
  //           color: Colors.red,
  //           fontWeight: FontWeight.bold,
  //           fontSize: 30,
  //         ),
  //         children: <TextSpan>[
  //           TextSpan(text: " : " + txt2, style: TextStyle(fontWeight: FontWeight.normal, fontSize: 25))
  //         ]),
  //   );
  // }

  // Widget exemple(txt) {
  //   return Row(
  //     children: [
  //       Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: [Text("ex :  ", style: TextStyle(fontSize: 18))],
  //       ),
  //       Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: [Text(txt, style: TextStyle(fontSize: 18))],
  //       )
  //     ],
  //   );
  // }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }
}
