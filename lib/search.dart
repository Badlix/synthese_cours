import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'PageAffi.dart';

class SearchPage extends StatefulWidget {
  SearchPage(this.changeTheme, this.mapFichier);
  final Function changeTheme;
  final Map mapFichier;
  @override
  _SearchPage createState() {
    return new _SearchPage();
  }
}

class _SearchPage extends State<SearchPage> {
  TextEditingController txtController = new TextEditingController();
  String txt = '';
  String page;
  // List<List<String>> allPages = [
  //   ['Atome', 'atome'],
  //   ['Racine Carré', 'racineCarre'],
  //   ['Le pH', 'ph']
  // ];
  List<List<String>> allPages;
  @override
  Widget build(BuildContext context) {
    loadAllPagesName();
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: TextFormField(
            controller: txtController,
            cursorColor: Colors.grey[300],
            showCursor: true,
            decoration: InputDecoration(
                hintText: ' ex: Atome',
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                )),
            onChanged: (text) {
              textChange();
            },
          ),
        ),
        actions: <Widget>[
          Icon(Icons.wb_sunny),
          Switch(
            value: isSwitched(),
            onChanged: (value) {
              widget.changeTheme();
            },
            activeTrackColor: Colors.grey,
            activeColor: Colors.grey[200],
            inactiveTrackColor: Colors.grey[200],
            inactiveThumbColor: Colors.grey,
          ),
          Icon(Icons.brightness_3)
        ],
      ),
      body: SingleChildScrollView(
        child: listTileList(txt),
      ),
    );
  }

  loadAllPagesName() {
    allPages = [];
    for (var i = 0; i < widget.mapFichier.keys.toList().length; i++) {
      String matiere = widget.mapFichier.keys.toList()[i];
      for (var j = 0; j < widget.mapFichier[matiere].keys.toList().length; j++) {
        String categorie = widget.mapFichier[matiere].keys.toList()[j];
        for (var k = 0; k < widget.mapFichier[matiere][categorie].keys.toList().length; k++) {
          allPages.add([
            widget.mapFichier[matiere][categorie].keys.toList()[k],
            widget.mapFichier[matiere][categorie].values.toList()[k]
          ]);
        }
      }
    }
  }

  listTileList(String txt) {
    List<ListTile> listTileList = [];
    for (var i = 0; i < allPages.length; i++) {
      //page = [];
      if (allPages[i][0].contains(txt))
        listTileList.add(
          ListTile(
            title: Text(allPages[i][0]),
            onTap: () {
              page = allPages[i][1];

              addStringToSF(page);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => PageAffi(widget.changeTheme, "", "", page)));
            },
          ),
        );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: listTileList,
    );
  }

  addStringToSF(txt) async {
    //SF = SharedPeference
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{}; // set initial values here if desired
      }
      return null;
    });
    SharedPreferences backup = await SharedPreferences.getInstance();
    backup.setString('searchBackup', txt);
  }

  textChange() {
    setState(() {
      txt = txtController.text;
    });
    print(txt);
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }
}
