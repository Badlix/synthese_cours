import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ConvertBinaire extends StatefulWidget {
  static const routeName = 'convertisseur binaire';
  ConvertBinaire(this.changeTheme);
  final Function changeTheme;
  @override
  _ConvertBinaire createState() {
    return new _ConvertBinaire();
  }
}

class _ConvertBinaire extends State<ConvertBinaire> {
  String nombreBinaire = "00000000";
  int resultatEnNombre = 0;
  int resulatEnLettre = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Convertisseur - Binaire"),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            Switch(
              value: isSwitched(),
              onChanged: (value) {
                widget.changeTheme();
              },
              activeTrackColor: Colors.grey,
              activeColor: Colors.grey[200],
              inactiveTrackColor: Colors.grey[200],
              inactiveThumbColor: Colors.grey,
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Card(
                color: whichColor(),
                child: Row(
                  children: _childrenNombreBinaire(1),
                )),
            Card(
              color: whichColor(),
              child: Row(children: _childrenNombreBinaire(2)),
            ),
            Text(" = " + resultatEnNombre.toString(), style: TextStyle(fontSize: 50)),
            Text("\n a = 97  <->  z = 122", style: TextStyle(fontSize: 20)),
            Text(" A = 65  <->  Z = 90", style: TextStyle(fontSize: 20)),
          ],
        ));
  }

  _childrenNombreBinaire(int nbRow) {
    List<Widget> _children = [];
    int debutIndex;
    double width = MediaQuery.of(context).size.width;
    if (nbRow == 1) {
      debutIndex = 0;
    } else {
      debutIndex = 4;
    }
    _children.add(Spacer(flex: 1));
    for (var i = debutIndex; i < 4 * nbRow; i++) {
      _children.add(Card(
          color: whichColor(),
          elevation: 0.5,
          child: Column(
            children: [
              Text(nombreBinaire[i], style: TextStyle(fontSize: width * 0.2)),
              Text(pow(2, 7 - i).toString()),
              Text("2^" + (7 - i).toString()),
              OutlinedButton(
                  onPressed: () {
                    changeDigit(i);
                  },
                  child: Icon(Icons.repeat, size: 12)),
            ],
          )));

      _children.add(Spacer(flex: 1));
    }

    return _children;
  }

  changeDigit(int digit) {
    String newChar;
    if (nombreBinaire[digit] == "0") {
      newChar = "1";
    } else {
      newChar = "0";
    }
    resultatEnNombre = 0;

    setState(() {
      nombreBinaire = nombreBinaire.substring(0, digit) + newChar + nombreBinaire.substring(digit + 1);
      for (var i = 0; i < 8; i++) {
        if (nombreBinaire[i] == "1") {
          resultatEnNombre += pow(2, 7 - i);
        }
      }
    });
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }

  whichColor() {
    return (Theme.of(context).brightness == Brightness.dark) ? Colors.grey[800] : Colors.grey[400];
  }
}

//mettre la trad des chiffre en num et en lettre
