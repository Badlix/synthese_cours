import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//Math / Arrondi et Troncature
class MathAutre1 extends StatefulWidget {
  static const routeName = 'MathAutre';
  MathAutre1(this.changeTheme);
  final Function changeTheme;

  @override
  _MathAutre1 createState() {
    return new _MathAutre1();
  }
}

class _MathAutre1 extends State<MathAutre1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Math'),
        actions: <Widget>[
          Icon(Icons.wb_sunny),
          Switch(
            value: isSwitched(),
            onChanged: (value) {
              widget.changeTheme();
            },
            activeTrackColor: Colors.grey,
            activeColor: Colors.grey[200],
            inactiveTrackColor: Colors.grey[200],
            inactiveThumbColor: Colors.grey,
          ),
          Icon(Icons.brightness_3)
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          RichText(
              text: TextSpan(
                  text: 'Def Troncature : ',
                  style: TextStyle(backgroundColor: Colors.green, fontWeight: FontWeight.bold, height: 1.3),
                  children: <TextSpan>[
                TextSpan(
                    text: "La troncature d'un nombre décimal est égales à sa partie entière",
                    style: TextStyle(backgroundColor: Colors.green, fontWeight: FontWeight.normal)),
                TextSpan(text: '\n      ', style: TextStyle(backgroundColor: Colors.transparent)),
                TextSpan(text: "=> la troncature de 32,87 est 32", style: TextStyle(backgroundColor: Colors.red, fontWeight: FontWeight.normal))
              ])),
          RichText(
              text: TextSpan(
                  text: '\nDef Arrondi : ',
                  style: TextStyle(backgroundColor: Colors.green, fontWeight: FontWeight.bold, height: 1.3),
                  children: <TextSpan>[
                TextSpan(
                    text:
                        "L'arrondi à l'unité d'un nombre décimal est égal au nombre entier le plus proche. Si le chiffre des dixièmes est égal à 5, on arrondit au nombre entier supérieur.",
                    style: TextStyle(backgroundColor: Colors.green, fontWeight: FontWeight.normal)),
                TextSpan(text: '\n      ', style: TextStyle(backgroundColor: Colors.transparent)),
                TextSpan(text: "=> l'arrondi à l'unité de 2,5 est 3", style: TextStyle(backgroundColor: Colors.red, fontWeight: FontWeight.normal))
              ])),
          RichText(
            text: TextSpan(
                text: '\n\nA ne pas confondre:',
                style: TextStyle(backgroundColor: Colors.yellow[700], fontWeight: FontWeight.bold, height: 1.3, color: Colors.black),
                children: <TextSpan>[
                  TextSpan(text: '\n- La troncature de 56,87 est 56.'),
                  TextSpan(text: "\n- L'arrondi de 56,87 est 57."),
                  TextSpan(text: "\nLa valeur approchée à l'unité par défaut de 56,87 est 56."),
                  TextSpan(text: "\nLa valeur approchée à l'unité par excès de 56,87 est 57.")
                ]),
          ),
        ],
      ),
    );
  }

  whatText() {
    return (Theme.of(context).brightness == Brightness.dark) ? Colors.grey[400] : Colors.black;
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }
}

class MathAutrePagePeriode extends StatefulWidget {
  static const routeName = 'MathAutrePeriode';
  MathAutrePagePeriode(this.changeTheme);
  final Function changeTheme;

  @override
  _MathAutrePagePeriode createState() {
    return new _MathAutrePagePeriode();
  }
}

class _MathAutrePagePeriode extends State<MathAutrePagePeriode> {
  var themeForImage = 'L';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Math'),
          actions: <Widget>[
            Icon(Icons.wb_sunny),
            Switch(
              value: isSwitched(),
              onChanged: (value) {
                widget.changeTheme();
              },
              activeTrackColor: Colors.grey,
              activeColor: Colors.grey[200],
              inactiveTrackColor: Colors.grey[200],
              inactiveThumbColor: Colors.grey,
            ),
            Icon(Icons.brightness_3)
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "\n Periode d'un nombre rationnel",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.red, backgroundColor: Colors.yellow[600]),
              ),
              Card(
                elevation: 0,
                color: Colors.transparent,
                margin: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                child: Text(
                    "=> Dans l'écriture d'un nombre rationel en notation décimale, groupe de chiffres qui se répète dans la partie décimale de ce nombre",
                    style: TextStyle(color: colorText())),
              ),
              Container(child: whatImage('atome')),
              Text('\n\n')
            ],
          ),
        ));
  }

  isSwitched() {
    return (Theme.of(context).brightness == Brightness.dark) ? true : false;
  }

  colorText() {
    return (Theme.of(context).brightness == Brightness.dark) ? Colors.white : Colors.black;
  }

  whatImage(var nomImage) {
    var whatTheme;
    if (Theme.of(context).brightness == Brightness.dark) {
      whatTheme = 'D';
    } else {
      whatTheme = 'L';
    }
    return Image(
      image: AssetImage('assets/$nomImage$whatTheme.gif'),
    );
  }
}
